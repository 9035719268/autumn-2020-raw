import math
import matplotlib.pyplot as plt
import numpy as np


def __getTecuToMetersCoefficient() -> float:
    l1: float = 1_575_420_000
    oneTecUnit: float = 1E16
    coefficient: float = 40.3 / pow(l1, 2) * oneTecUnit
    return coefficient


def __getDelayInTecu(weightMatrix: list, tecList: list) -> float:
    delay: float = 0
    for observation in range(4):
        weight: float = weightMatrix[observation]
        rawTec: float = tecList[observation]
        tecInOneTecUnit: float = rawTec * 0.1
        delay += (weight * tecInOneTecUnit)
    return delay


def getDelayInMeters(weightMatrix: list, tec: list) -> float:
    tecuToMetersCoefficient: float = __getTecuToMetersCoefficient()
    delayInTecu: float = __getDelayInTecu(weightMatrix, tec)
    delayInMeters: float = delayInTecu * tecuToMetersCoefficient
    return delayInMeters


def getKlobucharDelayInMeters(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float,
                              alpha: list, beta: list) -> float:
    delayInSeconds: float = __getKlobucharDelayInSeconds(latpp, lonpp, elevationAngle, azimuth, gpsTime, alpha, beta)
    speedOfLight: float = 2.99792458 * 1E8
    delayInMeters: float = delayInSeconds * speedOfLight
    return delayInMeters


def __getEarthCenteredAngle(elevationAngle: float) -> float:
    earthCenteredAngle: float = 0.0137 / (elevationAngle + 0.11) - 0.022
    return earthCenteredAngle


def __getIppLatitude(latpp: float, elevationAngle: float, azimuth: float) -> float:
    earthCenteredAngle: float = __getEarthCenteredAngle(elevationAngle)
    ippLatitude: float = latpp + earthCenteredAngle * math.cos(azimuth)
    if ippLatitude > 0.416:
        ippLatitude = 0.416
    elif ippLatitude < -0.416:
        ippLatitude = -0.416
    return ippLatitude


def __getIppLongtitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float) -> float:
    earthCenteredAngle: float = __getEarthCenteredAngle(elevationAngle)
    ippLatitude: float = __getIppLatitude(latpp, elevationAngle, azimuth)
    ippLongtitude: float = lonpp + (earthCenteredAngle * math.sin(azimuth) / (math.cos(ippLatitude)))
    return ippLongtitude


def __getIppGeomagneticLatitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float) -> float:
    ippLatitude: float = __getIppLatitude(latpp, elevationAngle, azimuth)
    ippLongtitude: float = __getIppLongtitude(latpp, lonpp, elevationAngle, azimuth)
    ippGeomagneticLatitude: float = ippLatitude + 0.064 * math.cos(ippLongtitude - 1.617)
    return ippGeomagneticLatitude


def __getIppLocalTime(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float) -> float:
    secondsInOneDay: float = 86_400
    secondsInTwelveHours: float = 43_200
    ippLongtitude: float = __getIppLongtitude(latpp, lonpp, elevationAngle, azimuth)
    ippLocalTime: float = secondsInTwelveHours * ippLongtitude + gpsTime
    while ippLocalTime > secondsInOneDay:
        ippLocalTime -= secondsInOneDay
    while ippLocalTime < 0:
        ippLocalTime += secondsInOneDay
    return ippLocalTime


def __getIonosphericDelayAmplitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float,
                                   alpha: list) -> float:
    ippGeomagneticLatitude: float = __getIppGeomagneticLatitude(latpp, lonpp, elevationAngle, azimuth)
    amplitude: float = 0
    for i in range(4):
        amplitude += (alpha[i] * pow(ippGeomagneticLatitude, i))
    if amplitude < 0:
        amplitude = 0
    return amplitude


def __getIonosphericDelayPeriod(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, beta: list) -> float:
    ippGeomagneticLatitude: float = __getIppGeomagneticLatitude(latpp, lonpp, elevationAngle, azimuth)
    period: float = 0
    for i in range(4):
        period += (beta[i] * pow(ippGeomagneticLatitude, i))
    if period < 72000:
        period = 72000
    return period


def __getIonosphericDelayPhase(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float,
                               beta: list) -> float:
    ippLocalTime: float = __getIppLocalTime(latpp, lonpp, elevationAngle, azimuth, gpsTime)
    ionosphericDelayPeriod: float = __getIonosphericDelayPeriod(latpp, lonpp, elevationAngle, azimuth, beta)
    ionosphericDelayPhase: float = 2 * math.pi * (ippLocalTime - 50_400) / ionosphericDelayPeriod
    return ionosphericDelayPhase


def __getSlantFactor(elevationAngle: float) -> float:
    slantFactor: float = 1 + 16 * math.pow((0.53 - elevationAngle), 3)
    return slantFactor


def __getKlobucharDelayInSeconds(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float,
                                 alpha: list, beta: list) -> float:
    ionosphericDelayPhase: float = __getIonosphericDelayPhase(latpp, lonpp, elevationAngle, azimuth, gpsTime, beta)
    ionosphericDelayAmplitude: float = __getIonosphericDelayAmplitude(latpp, lonpp, elevationAngle, azimuth, alpha)
    slantFactor: float = __getSlantFactor(elevationAngle)
    ionosphericTimeDelay: float
    if abs(ionosphericDelayPhase) > 1.57:
        ionosphericTimeDelay = 5E-9 * slantFactor
    else:
        ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - math.pow(ionosphericDelayPhase, 2) / 2 +
                                                                    math.pow(ionosphericDelayPhase, 4) / 24)) * slantFactor
    return ionosphericTimeDelay


def createDelays(tecA1: list, tecA2: list, tecA3: list, tecA4: list, weightMatrix: list,
                 amountOfObservations: int) -> list:
    delays: list = []
    for observation in range(amountOfObservations):
        a1: int = tecA1[observation]
        a2: int = tecA2[observation]
        a3: int = tecA3[observation]
        a4: int = tecA4[observation]

        tec: list = []
        tec.extend([a1, a2, a3, a4])

        delay: float = getDelayInMeters(weightMatrix, tec)
        delays.append(delay)
    return delays


def createKlobuchar(latpp: float, lonpp: float,
                    elevationAngle: float, azimuth: float,
                    alpha: list, beta: list, gpsTime: list,
                    amountOfObservations: int) -> list:
    models: list = []
    for observation in range(amountOfObservations):
        time: float = gpsTime[observation]
        model: float = getKlobucharDelayInMeters(latpp, lonpp, elevationAngle, azimuth, time, alpha, beta)
        models.append(model)
    return models


def printDelays(forecastDelays: list, preciseDelays: list, klobucharDelays: list, amountOfObservations: int) -> None:
    print("igrg\tigsg\tklobuchar")
    for observation in range(amountOfObservations):
        forecastValue: float = forecastDelays[observation]
        preciseValue: float = preciseDelays[observation]
        klobucharValue: float = klobucharDelays[observation]
        print(str(round(forecastValue, 3)) + "\t" + str(round(preciseValue, 3)) + "\t" + str(round(klobucharValue, 3)))


def showDelays(forecastDelays: list, preciseDelays: list, klobucharDelays: list, amountOfObservations: int) -> None:
    forecastValues: list = []
    preciseValues: list = []
    klobucharValues: list = []
    observations = np.arange(0, amountOfObservations)
    for observation in range(amountOfObservations):
        forecastValue: float = forecastDelays[observation]
        preciseValue: float = preciseDelays[observation]
        klobucharValue: float = klobucharDelays[observation]
        forecastValues.append(forecastValue)
        preciseValues.append(preciseValue)
        klobucharValues.append(klobucharValue)
    plt.plot(observations * 2, forecastValues, 'o-', label="igrg")
    plt.plot(observations * 2, preciseValues, 'o-', label="igsg")
    plt.plot(observations * 2, klobucharValues, 'o-', label="Klobuchar")
    plt.locator_params(axis='x', nbins=24)
    plt.xlabel("Время, час")
    plt.ylabel("Ионосферная поправка, метр")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def main():
    amountOfObservations: int = 12

    halfCircle: float = 180

    latpp: float = 61 / halfCircle
    lonpp: float = 50 / halfCircle

    lon1: float = 45 / halfCircle
    lon2: float = 55 / halfCircle
    lat1: float = 55 / halfCircle
    lat2: float = 65 / halfCircle

    xpp: float = (lonpp - lon1) / (lon2 - lon1)
    ypp: float = (latpp - lat1) / (lat2 - lat1)

    weightMatrix: list = [0] * 4
    weightMatrix[0] = xpp * ypp
    weightMatrix[1] = (1 - xpp) * ypp
    weightMatrix[2] = (1 - xpp) * (1 - ypp)
    weightMatrix[3] = xpp * (1 - ypp)
    print(weightMatrix)

    alpha: list = [0.7451E-08, -0.1490E-07, -0.5960E-07, 0.1192E-06]
    beta: list = [0.9216E+05, -0.1147E+06, -0.1311E+06, 0.7209E+06]
    gpsTime: list = [
        80496.0, 90270.0, 93630.0, 100830.0, 108030.0, 0.0, 124230.0, 129630.0, 136800.0, 144000.0, 151200.0, 158400.0
    ]

    forecastA1: list = [26, 37, 22, 32, 51, 62, 38, 18, 18, 11, 12, 13]
    forecastA2: list = [27, 31, 20, 21, 42, 60, 39, 21, 18, 11, 13, 15]
    forecastA3: list = [41, 36, 33, 60, 78, 95, 76, 58, 47, 42, 40, 46]
    forecastA4: list = [41, 37, 38, 73, 81, 101, 76, 55, 47, 49, 43, 52]

    preciseA1: list = [32, 39, 24, 30, 43, 60, 36, 14, 10, 6, 6, 7]
    preciseA2: list = [31, 41, 25, 23, 38, 61, 37, 19, 10, 6, 7, 8]
    preciseA3: list = [39, 35, 30, 57, 76, 100, 77, 56, 41, 39, 41, 42]
    preciseA4: list = [34, 31, 34, 67, 76, 97, 68, 43, 39, 40, 41, 43]

    forecastDelays: list = createDelays(forecastA1, forecastA2, forecastA3, forecastA4,
                                        weightMatrix, amountOfObservations)
    preciseDelays: list = createDelays(preciseA1, preciseA2, preciseA3, preciseA4, weightMatrix, amountOfObservations)

    elevationAngle: float = 90 / halfCircle
    azimuth: float = 0
    klobucharDelays: list = createKlobuchar(latpp, lonpp, elevationAngle, azimuth, alpha, beta, gpsTime,
                                            amountOfObservations)

    printDelays(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)
    showDelays(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)


if __name__ == "__main__":
    main()

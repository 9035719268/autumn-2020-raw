import math
import matplotlib.pyplot as plt
import numpy as np


def readBytes(fileName: str) -> bytes:
    with open(fileName, "rb") as file:
        allBytes: bytes = file.read()
    return allBytes


def __analyzeSyntaxAndReturnLinesList(allBytes: bytes) -> list:
    lines: list = []
    words: list = []
    symbols: list = []
    isWord: bool = False

    newLine: int = 10
    space: int = 32

    for symbol in allBytes:
        if symbol is newLine:
            words.append(symbols)
            symbols = []
            lines.append(words)
            words = []
            isWord = False
        elif isWord is True and symbol is space:
            words.append(symbols)
            symbols = []
            isWord = False
        elif symbol is not space:
            isWord = True
            symbols.append(symbol)
    return lines


def extractIonCoeffs(allBytes: bytes, lineNumber: int) -> list:
    lines: list = __analyzeSyntaxAndReturnLinesList(allBytes)
    line: list = lines[lineNumber]
    coeffs: list = []
    for coefficient in range(4):
        numeric: float = __getCoeffNumeric(line, coefficient)
        coeffs.append(numeric)
    return coeffs


def __getCoeffNumeric(line: list, number: int) -> float:
    numberBuilder: str = ""
    digits: int = len(line[number])
    for digit in range(digits):
        symbol: chr = chr(line[number][digit])
        if symbol is 'D':
            numberBuilder += 'E'
        else:
            numberBuilder += symbol
    numeric: float = float(numberBuilder)
    return numeric


def getGpsTime(allBytes: bytes, amountOfObservations: int, requiredSatelliteNumberChar: str,
               requiredSatelliteNumberSize: int, requiredSatelliteNumber2Char: str = None) -> list:
    lines: list = __analyzeSyntaxAndReturnLinesList(allBytes)
    gpsTime: list = [0] * amountOfObservations
    startOfObservations: int = 8
    linesPerObservations: int = 8
    asciiStarts: int = 48

    for observation in range(startOfObservations, len(lines), linesPerObservations):
        try:
            minutesFirstNumber: int = lines[observation][5][0] - asciiStarts
            minutesNumberSize: int = len(lines[observation][5])
            if minutesFirstNumber is 0 and minutesNumberSize is 1:
                if requiredSatelliteNumber2Char is not None:
                    requiredSatelliteNumber: int = __toAscii(requiredSatelliteNumberChar)
                    requiredSatelliteNumber2: int = __toAscii(requiredSatelliteNumber2Char)
                    satelliteNumber: int = lines[observation][0][0]
                    satelliteNumber2: int = lines[observation][0][1]
                    satelliteNumberSize = len(lines[observation][0])
                    hourFirstNumber: int = lines[observation][4][0] - asciiStarts
                    if (len(lines[observation][4]) is 1 and
                            satelliteNumber is requiredSatelliteNumber and
                            satelliteNumber2 is requiredSatelliteNumber2 and
                            satelliteNumberSize is requiredSatelliteNumberSize):
                        numeric: float = __getGpsTimeNumeric(lines, observation)
                        gpsTime[int(hourFirstNumber / 2)] = numeric
                    else:
                        hourSecondNumber: int = lines[observation][4][1] - asciiStarts
                        hourFull: str = str(hourFirstNumber) + str(hourSecondNumber)
                        hourValue: int = int(hourFull)

                        if (satelliteNumber is requiredSatelliteNumber and
                                satelliteNumber2 is requiredSatelliteNumber2 and
                                satelliteNumberSize is requiredSatelliteNumberSize):
                            numeric: float = __getGpsTimeNumeric(lines, observation)
                            gpsTime[int(hourValue / 2)] = numeric
                else:
                    requiredSatelliteNumber: int = __toAscii(requiredSatelliteNumberChar)
                    satelliteNumber: int = lines[observation][0][0]
                    satelliteNumberSize = len(lines[observation][0])
                    hourFirstNumber: int = lines[observation][4][0] - asciiStarts
                    if (len(lines[observation][4]) is 1 and
                            satelliteNumber is requiredSatelliteNumber and
                            satelliteNumberSize is requiredSatelliteNumberSize):
                        numeric: float = __getGpsTimeNumeric(lines, observation)
                        gpsTime[int(hourFirstNumber / 2)] = numeric
                    else:
                        hourSecondNumber: int = lines[observation][4][1] - asciiStarts
                        hourFull: str = str(hourFirstNumber) + str(hourSecondNumber)
                        hourValue: int = int(hourFull)

                        if (satelliteNumber is requiredSatelliteNumber and
                                satelliteNumberSize is requiredSatelliteNumberSize):
                            numeric: float = __getGpsTimeNumeric(lines, observation)
                            gpsTime[int(hourValue / 2)] = numeric
        except Exception:
            pass
    return gpsTime


def __getGpsTimeNumeric(lines: list, observation: int) -> float:
    numberBuilder: str = ""
    observationOffset: int = 7
    digits = len(lines[observation + observationOffset][0])

    for digit in range(digits):
        symbol: chr = chr(lines[observation + observationOffset][0][digit])
        if symbol is 'D':
            numberBuilder += 'E'
        else:
            numberBuilder += symbol
    numeric: float = float(numberBuilder)
    return numeric


def __toAscii(number: str) -> int:
    return int(number) + 48


def getTecArray(allBytes: bytes, firstLine: int,
                requiredFirstLatDigitChar: str, requiredSecondLatDigitChar: str, requiredThirdLatDigitChar: str) -> list:
    requiredFirstLatDigit: int = 0
    requiredSecondLatDigit: int = __toAscii(requiredSecondLatDigitChar)
    requiredThirdLatDigit: int = 0
    if requiredFirstLatDigitChar is "-":
        requiredFirstLatDigit = 45
    else:
        requiredFirstLatDigit = __toAscii(requiredFirstLatDigitChar)

    if requiredThirdLatDigitChar is ".":
        requiredThirdLatDigit = 46
    else:
        requiredThirdLatDigit = __toAscii(requiredThirdLatDigitChar)

    lines: list = __analyzeSyntaxAndReturnLinesList(allBytes)
    tecArray: list = []
    amountOfLinesWithTec: int = firstLine + 5575
    for line in range(firstLine, amountOfLinesWithTec):
        try:
            firstDigitOfLat: int = lines[line][0][0]
            secondDigitOfLat: int = lines[line][0][1]
            thirdDigitOfLat: int = lines[line][0][2]
            linesWithTecPerLat: int = 5
            if (firstDigitOfLat is requiredFirstLatDigit and
                    secondDigitOfLat is requiredSecondLatDigit and
                    thirdDigitOfLat is requiredThirdLatDigit):
                tecPerLat: list = []
                for lineWithTec in range(1, linesWithTecPerLat + 1):
                    numbersLine: list = __getNumberLine(lines, line, lineWithTec)
                    tecPerLat.append(numbersLine)
                tecArray.append(tecPerLat)
        except Exception:
            pass
    return tecArray


def __getNumberLine(lines: list, line: int, lineWithTec: int) -> list:
    numbersLine: list = []
    numbersInRow: int = len(lines[line + lineWithTec])
    for number in range(numbersInRow):
        numeric: int = __getNumeric(lines, line, lineWithTec, number)
        numbersLine.append(numeric)
    return numbersLine


def __getNumeric(lines: list, line: int, lineWithTec: int, number: int) -> int:
    numberBuilder: str = ""
    numberLength: int = len(lines[line + lineWithTec][number])
    for digit in range(numberLength):
        symbol: chr = chr(lines[line + lineWithTec][number][digit])
        numberBuilder += symbol
    numeric: int = int(numberBuilder)
    return numeric


def __getDelayInMeters(weightMatrix: list, tec: list) -> float:
    tecuToMetersCoefficient: float = __getTecuToMetersCoefficient()
    delayInTecu: float = __getDelayInTecu(weightMatrix, tec)
    delayInMeters: float = delayInTecu * tecuToMetersCoefficient
    return delayInMeters


def __getTecuToMetersCoefficient():
    l1: float = 1_575_420_000
    oneTecUnit: float = 1E16
    coefficient: float = 40.3 / pow(l1, 2) * oneTecUnit
    return coefficient


def __getDelayInTecu(weightMatrix: list, tecList: list) -> float:
    delay: float = 0
    for observation in range(4):
        weight: float = weightMatrix[observation]
        rawTec: float = tecList[observation]
        tecInOneTecUnit: float = rawTec * 0.1
        delay += (weight * tecInOneTecUnit)
    return delay


def __getEarthCenteredAngle(elevationAngle: float) -> float:
    earthCenteredAngle: float = 0.0137 / (elevationAngle + 0.11) - 0.022
    return earthCenteredAngle


def __getIppLatitude(latpp: float, elevationAngle: float, azimuth: float) -> float:
    earthCenteredAngle: float = __getEarthCenteredAngle(elevationAngle)
    ippLatitude: float = latpp + earthCenteredAngle * math.cos(azimuth)
    if ippLatitude > 0.416:
        ippLatitude = 0.416
    elif ippLatitude < -0.416:
        ippLatitude = -0.416
    return ippLatitude


def __getIppLongtitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float) -> float:
    earthCenteredAngle: float = __getEarthCenteredAngle(elevationAngle)
    ippLatitude: float = __getIppLatitude(latpp, elevationAngle, azimuth)
    ippLongtitude: float = lonpp + (earthCenteredAngle * math.sin(azimuth) / (math.cos(ippLatitude)))
    return ippLongtitude


def __getIppGeomagneticLatitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float) -> float:
    ippLatitude: float = __getIppLatitude(latpp, elevationAngle, azimuth)
    ippLongtitude: float = __getIppLongtitude(latpp, lonpp, elevationAngle, azimuth)
    ippGeomagneticLatitude: float = ippLatitude + 0.064 * math.cos(ippLongtitude - 1.617)
    return ippGeomagneticLatitude


def __getIppLocalTime(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float) -> float:
    secondsInOneDay: float = 86_400
    secondsInTwelveHours: float = 43_200
    ippLongtitude: float = __getIppLongtitude(latpp, lonpp, elevationAngle, azimuth)
    ippLocalTime: float = secondsInTwelveHours * ippLongtitude + gpsTime
    while ippLocalTime > secondsInOneDay:
        ippLocalTime -= secondsInOneDay
    while ippLocalTime < 0:
        ippLocalTime += secondsInOneDay
    return ippLocalTime


def __getIonosphericDelayAmplitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float,
                                   alpha: list) -> float:
    ippGeomagneticLatitude: float = __getIppGeomagneticLatitude(latpp, lonpp, elevationAngle, azimuth)
    amplitude: float = 0
    for i in range(4):
        amplitude += (alpha[i] * pow(ippGeomagneticLatitude, i))
    if amplitude < 0:
        amplitude = 0
    return amplitude


def __getIonosphericDelayPeriod(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, beta: list) -> float:
    ippGeomagneticLatitude: float = __getIppGeomagneticLatitude(latpp, lonpp, elevationAngle, azimuth)
    period: float = 0
    for i in range(4):
        period += (beta[i] * pow(ippGeomagneticLatitude, i))
    if period < 72_000:
        period = 72_000
    return period


def __getIonosphericDelayPhase(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float,
                               beta: list) -> float:
    ippLocalTime: float = __getIppLocalTime(latpp, lonpp, elevationAngle, azimuth, gpsTime)
    ionosphericDelayPeriod: float = __getIonosphericDelayPeriod(latpp, lonpp, elevationAngle, azimuth, beta)
    ionosphericDelayPhase: float = 2 * math.pi * (ippLocalTime - 50_400) / ionosphericDelayPeriod
    return ionosphericDelayPhase


def __getSlantFactor(elevationAngle: float) -> float:
    slantFactor: float = 1 + 16 * math.pow((0.53 - elevationAngle), 3)
    return slantFactor


def __getKlobucharDelayInSeconds(latpp: float, lonpp: float,
                                 elevationAngle: float, azimuth: float, gpsTime: float,
                                 alpha: list, beta: list) -> float:
    ionosphericDelayPhase: float = __getIonosphericDelayPhase(latpp, lonpp, elevationAngle, azimuth, gpsTime, beta)
    ionosphericDelayAmplitude: float = __getIonosphericDelayAmplitude(latpp, lonpp, elevationAngle, azimuth, alpha)
    slantFactor: float = __getSlantFactor(elevationAngle)
    ionosphericTimeDelay: float
    if abs(ionosphericDelayPhase) > 1.57:
        ionosphericTimeDelay = 5E-9 * slantFactor
    else:
        ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - math.pow(ionosphericDelayPhase, 2) / 2 +
                                                                    math.pow(ionosphericDelayPhase, 4) / 24)) * slantFactor
    return ionosphericTimeDelay


def getKlobucharDelayInMeters(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float,
                              alpha: list, beta: list) -> float:
    delayInSeconds: float = __getKlobucharDelayInSeconds(latpp, lonpp, elevationAngle, azimuth, gpsTime, alpha, beta)
    speedOfLight: float = 2.99792458 * 1E8
    delayInMeters: float = delayInSeconds * speedOfLight
    return delayInMeters


def getIonosphericDelays(tecA1: list, tecA2: list, tecA3: list, tecA4: list,
                         lon1: int, lon2: int, lon3: int, lon4: int,
                         weightMatrix: list, amountOfObservations: int) -> list:
    lonFirst: int = -180
    dlon: int = 5
    tecValuesPerLine: int = 16
    delays: list = []

    rowA1: int = __getRow(lonFirst, dlon, tecValuesPerLine, lon1)
    rowA2: int = __getRow(lonFirst, dlon, tecValuesPerLine, lon2)
    rowA3: int = __getRow(lonFirst, dlon, tecValuesPerLine, lon3)
    rowA4: int = __getRow(lonFirst, dlon, tecValuesPerLine, lon4)

    posA1: int = __getPos(lonFirst, dlon, tecValuesPerLine, rowA1, lon1)
    posA2: int = __getPos(lonFirst, dlon, tecValuesPerLine, rowA2, lon2)
    posA3: int = __getPos(lonFirst, dlon, tecValuesPerLine, rowA3, lon3)
    posA4: int = __getPos(lonFirst, dlon, tecValuesPerLine, rowA4, lon4)

    for observation in range(amountOfObservations):
        a1: int = tecA1[observation][rowA1][posA1]
        a2: int = tecA2[observation][rowA2][posA2]
        a3: int = tecA3[observation][rowA3][posA3]
        a4: int = tecA4[observation][rowA4][posA4]

        tec: list = []
        tec.extend([a1, a2, a3, a4])

        delay: float = __getDelayInMeters(weightMatrix, tec)
        delays.append(delay)
    return delays


def __getPos(lonFirst: int, dlon: int, tecValuesPerLine: int, row: int, lon: int) -> int:
    number: int = int(abs(((lonFirst - lon) / dlon)) - (row * tecValuesPerLine))
    return number


def __getRow(lonFirst: int, dlon: int, tecValuesPerLine: int, lon: int) -> int:
    row: int = int(abs((lonFirst - lon) / (tecValuesPerLine * dlon)))
    return row


def getKlobuchar(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, alpha: list, beta: list,
                 gpsTime: list, amountOfObservations: int) -> list:
    models: list = []
    for observation in range(amountOfObservations):
        time: float = gpsTime[observation]
        model: float = getKlobucharDelayInMeters(latpp, lonpp, elevationAngle, azimuth, time, alpha, beta)
        models.append(model)
    return models


def printDelays(forecastDelays: list, preciseDelays: list, klobucharDelays: list, amountOfObservations: int):
    print("igrg\tigsg\tklobuchar")
    for observation in range(amountOfObservations):
        forecastValue: float = forecastDelays[observation]
        preciseValue: float = preciseDelays[observation]
        klobucharValue: float = klobucharDelays[observation]
        print(str(round(forecastValue, 3)) + "\t" + str(round(preciseValue, 3)) + "\t" + str(round(klobucharValue, 3)))


def showDelays(forecastDelays: list, preciseDelays: list, klobucharDelays: list, amountOfObservations: int):
    observations = np.arange(0, amountOfObservations)
    plt.plot(observations * 2, forecastDelays, 'o-', label="igrg")
    plt.plot(observations * 2, preciseDelays, 'o-', label="igsg")
    plt.plot(observations * 2, klobucharDelays, 'o-', label="Klobuchar")
    plt.locator_params(axis='x', nbins=24)
    plt.xlabel("Время, час")
    plt.ylabel("Ионосферная поправка, метр")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def main():
    amountOfObservations: int = 12

    halfCircle: float = 180

    latpp: float = 71 / halfCircle
    lonpp: float = 128 / halfCircle

    lon1: float = 125 / halfCircle
    lon2: float = 130 / halfCircle
    lat1: float = 70 / halfCircle
    lat2: float = 72.5 / halfCircle

    xpp: float = (lonpp - lon1) / (lon2 - lon1)
    ypp: float = (latpp - lat1) / (lat2 - lat1)

    weightMatrix: list = [0] * 4
    weightMatrix[0] = xpp * ypp
    weightMatrix[1] = (1 - xpp) * ypp
    weightMatrix[2] = (1 - xpp) * (1 - ypp)
    weightMatrix[3] = xpp * (1 - ypp)

    fileNameEphemeris: str = "resources/brdc0010.18n"
    fileBytesEphemeris: bytes = readBytes(fileNameEphemeris)
    alpha: list = extractIonCoeffs(fileBytesEphemeris, 3)
    beta: list = extractIonCoeffs(fileBytesEphemeris, 4)
    gpsTime: list = getGpsTime(fileBytesEphemeris, amountOfObservations, "7", 1)

    fileNameForecast: str = "resources/igrg0010.18i"
    fileBytesForecast: bytes = readBytes(fileNameForecast)
    forecastA1: list = getTecArray(fileBytesForecast, 304, "7", "2", ".")
    forecastA2: list = getTecArray(fileBytesForecast, 304, "7", "2", ".")
    forecastA3: list = getTecArray(fileBytesForecast, 304, "7", "0", ".")
    forecastA4: list = getTecArray(fileBytesForecast, 304, "7", "0", ".")

    fileNamePrecise: str = "resources/igsg0010.18i"
    fileBytesPrecise: bytes = readBytes(fileNamePrecise)
    preciseA1: list = getTecArray(fileBytesPrecise, 394, "7", "2", ".")
    preciseA2: list = getTecArray(fileBytesPrecise, 394, "7", "2", ".")
    preciseA3: list = getTecArray(fileBytesPrecise, 394, "7", "0", ".")
    preciseA4: list = getTecArray(fileBytesPrecise, 394, "7", "0", ".")

    forecastDelays: list = getIonosphericDelays(forecastA1, forecastA2, forecastA3, forecastA4, 130, 125, 125, 130,
                                                weightMatrix, amountOfObservations)

    preciseDelays: list = getIonosphericDelays(preciseA1, preciseA2, preciseA3, preciseA4, 130, 125, 125, 130,
                                               weightMatrix, amountOfObservations)

    elevationAngle: float = 90 / halfCircle
    azimuth: float = 0
    klobucharDelays: list = getKlobuchar(latpp, lonpp, elevationAngle, azimuth, alpha, beta, gpsTime,
                                         amountOfObservations)

    printDelays(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)
    showDelays(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)


if __name__ == "__main__":
    main()

import math
import matplotlib.pyplot as plt
import numpy as np


class UserGeo:
    halfCircle: float = 180
    latpp: float = 48 / halfCircle
    lonpp: float = 135 / halfCircle


class IGPGeo:
    halfCircle: float = 180
    lon1: float = 130 / halfCircle
    lon2: float = 140 / halfCircle
    lat1: float = 45 / halfCircle
    lat2: float = 55 / halfCircle


class AxisGeo:
    def __init__(self, lonpp: float, lon1: float, lon2: float, latpp: float, lat1: float, lat2: float) -> None:
        self.__xpp: float = (lonpp - lon1) / (lon2 - lon1)
        self.__ypp: float = (latpp - lat1) / (lat2 - lat1)

    @property
    def xpp(self) -> float:
        return self.__xpp

    @property
    def ypp(self) -> float:
        return self.__ypp


class WeightMatrix:
    def __init__(self, axisGeo: AxisGeo):
        self.__weights: list = [0] * 4
        xpp: float = axisGeo.xpp
        ypp: float = axisGeo.ypp
        self.__weights[0]: float = xpp * ypp
        self.__weights[1]: float = (1 - xpp) * ypp
        self.__weights[2]: float = (1 - xpp) * (1 - ypp)
        self.__weights[3]: float = xpp * (1 - ypp)

    def __getitem__(self, pos) -> float:
        return self.__weights[pos]


class IonCoefficients:
    def __init__(self, coefficients: list) -> None:
        self.__coefficients: list = coefficients

    def __getitem__(self, pos) -> float:
        return self.__coefficients[pos]


class Tec:
    def __init__(self, tec: list) -> None:
        self.__tec: list = tec

    def __getitem__(self, pos: int) -> float:
        return self.__tec[pos]


class GpsTime:
    def __init__(self, gpsTime: list) -> None:
        self.__gpsTime: list = gpsTime

    def __getitem__(self, pos: int) -> float:
        return self.__gpsTime[pos]


class IonosphericDelay:
    def __init__(self, weightMatrix: WeightMatrix, tec: Tec) -> None:
        self.__weightMatrix: WeightMatrix = weightMatrix
        self.__tec: Tec = tec

    @property
    def delayInMeters(self) -> float:
        tecuToMetersCoefficient: float = self.__tecuToMetersCoefficient
        delayInTecu: float = self.__delayInTecu
        delayInMeters: float = delayInTecu * tecuToMetersCoefficient
        return delayInMeters

    @property
    def __tecuToMetersCoefficient(self) -> float:
        l1: float = 1_575_420_000
        oneTecUnit: float = 1E16
        coefficient: float = 40.3 / pow(l1, 2) * oneTecUnit
        return coefficient

    @property
    def __delayInTecu(self) -> float:
        delay: float = 0
        for observation in range(4):
            weight: float = self.__weightMatrix[observation]
            rawTec: float = self.__tec[observation]
            tecInOneTecUnit: float = rawTec * 0.1
            delay += (weight * tecInOneTecUnit)
        return delay


class KlobucharModel:
    def __init__(self, gpsTime: float, alpha: IonCoefficients, beta: IonCoefficients) -> None:
        halfCircle: float = 180
        self.__gpsTime: float = gpsTime
        self.__elevationAngle: float = 90 / halfCircle
        self.__azimuth: float = 0
        self.__alpha: IonCoefficients = alpha
        self.__beta: IonCoefficients = beta

    @property
    def klobucharDelayInMeters(self) -> float:
        delayInSeconds: float = self.__klobucharDelayInSeconds
        speedOfLight: float = 2.99792458 * 1E8
        delayInMeters: float = delayInSeconds * speedOfLight
        return delayInMeters

    @property
    def __earthCenteredAngle(self) -> float:
        earthCenteredAngle: float = 0.0137 / (self.__elevationAngle + 0.11) - 0.022
        return earthCenteredAngle

    @property
    def __ippLatitude(self) -> float:
        latpp: float = UserGeo.latpp
        earthCenteredAngle: float = self.__earthCenteredAngle
        ippLatitude: float = latpp + earthCenteredAngle * math.cos(self.__azimuth)
        if ippLatitude > 0.416:
            ippLatitude = 0.416
        elif ippLatitude < -0.416:
            ippLatitude = -0.416
        return ippLatitude

    @property
    def __ippLongtitude(self) -> float:
        lonpp: float = UserGeo.lonpp
        earthCenteredAngle: float = self.__earthCenteredAngle
        ippLatitude: float = self.__ippLatitude
        ippLongtitude: float = lonpp + (earthCenteredAngle * math.sin(self.__azimuth) / (math.cos(ippLatitude)))
        return ippLongtitude

    @property
    def __ippGeomagneticLatitude(self) -> float:
        ippLatitude: float = self.__ippLatitude
        ippLongtitude: float = self.__ippLongtitude
        ippGeomagneticLatitude: float = ippLatitude + 0.064 * math.cos(ippLongtitude - 1.617)
        return ippGeomagneticLatitude

    @property
    def __ippLocalTime(self) -> float:
        secondsInOneDay: float = 86_400
        secondsInTwelveHours: float = 43_200
        ippLongtitude: float = self.__ippLongtitude
        ippLocalTime: float = secondsInTwelveHours * ippLongtitude + self.__gpsTime
        while ippLocalTime > secondsInOneDay:
            ippLocalTime -= secondsInOneDay
        while ippLocalTime < 0:
            ippLocalTime += secondsInOneDay
        return ippLocalTime

    @property
    def __ionosphericDelayAmplitude(self) -> float:
        ippGeomagneticLatitude: float = self.__ippGeomagneticLatitude
        amplitude: float = 0
        for i in range(4):
            amplitude += (self.__alpha[i] * pow(ippGeomagneticLatitude, i))
        if amplitude < 0:
            amplitude = 0
        return amplitude

    @property
    def __ionosphericDelayPeriod(self) -> float:
        ippGeomagneticLatitude: float = self.__ippGeomagneticLatitude
        period: float = 0
        for i in range(4):
            period += (self.__beta[i] * pow(ippGeomagneticLatitude, i))
        if period < 72_000:
            period = 72_000
        return period

    @property
    def __ionosphericDelayPhase(self) -> float:
        ippLocalTime: float = self.__ippLocalTime
        ionosphericDelayPeriod: float = self.__ionosphericDelayPeriod
        ionosphericDelayPhase: float = 2 * math.pi * (ippLocalTime - 50_400) / ionosphericDelayPeriod
        return ionosphericDelayPhase

    @property
    def __slantFactor(self) -> float:
        slantFactor: float = 1 + 16 * math.pow((0.53 - self.__elevationAngle), 3)
        return slantFactor

    @property
    def __klobucharDelayInSeconds(self) -> float:
        ionosphericDelayPhase: float = self.__ionosphericDelayPhase
        ionosphericDelayAmplitude: float = self.__ionosphericDelayAmplitude
        slantFactor: float = self.__slantFactor
        ionosphericTimeDelay: float
        if abs(ionosphericDelayPhase) > 1.57:
            ionosphericTimeDelay = 5E-9 * slantFactor
        else:
            ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - math.pow(ionosphericDelayPhase, 2) / 2 +
                                                                        math.pow(ionosphericDelayPhase, 4) / 24)) * slantFactor
        return ionosphericTimeDelay


class IonosphericDelaysFactory:
    def __init__(self, weightMatrix: WeightMatrix, amountOfObservations: int):
        self.__amountOfObservations: int = amountOfObservations
        self.__weightMatrix: WeightMatrix = weightMatrix

    def createDelays(self, tecA1: list, tecA2: list, tecA3: list, tecA4: list) -> list:
        delays: list = []
        for observation in range(self.__amountOfObservations):
            a1: int = tecA1[observation]
            a2: int = tecA2[observation]
            a3: int = tecA3[observation]
            a4: int = tecA4[observation]

            tecArray: list = []
            tecArray.extend([a1, a2, a3, a4])

            tempTec = Tec(tecArray)
            tempDelay = IonosphericDelay(self.__weightMatrix, tempTec)
            delays.append(tempDelay)
        return delays


class KlobucharDelaysFactory:
    def __init__(self, gpsTime: GpsTime, alpha: IonCoefficients, beta: IonCoefficients,
                 amountOfObservations: int) -> None:
        self.__gpsTime: GpsTime = gpsTime
        self.__alpha: IonCoefficients = alpha
        self.__beta: IonCoefficients = beta
        self.__amountOfObservations: int = amountOfObservations

    def createKlobuchar(self) -> list:
        models: list = []
        for observation in range(self.__amountOfObservations):
            time: float = self.__gpsTime[observation]
            model: KlobucharModel = KlobucharModel(time, self.__alpha, self.__beta)
            models.append(model)
        return models


class ConsoleOutput:
    def __init__(self, forecastDelays: list, preciseDelays: list, klobucharDelays: list,
                 amountOfObservations: int) -> None:
        self.__forecastDelays: list = forecastDelays
        self.__preciseDelays: list = preciseDelays
        self.__klobucharDelays: list = klobucharDelays
        self.__amountOfObservations: int = amountOfObservations

    def printDelays(self) -> None:
        print("igrg\tigsg\tklobuchar")
        for observation in range(self.__amountOfObservations):
            forecastValue: float = self.__forecastDelays[observation].delayInMeters
            preciseValue: float = self.__preciseDelays[observation].delayInMeters
            klobucharValue: float = self.__klobucharDelays[observation].klobucharDelayInMeters
            print(str(round(forecastValue, 3)) + "\t\t" + str(round(preciseValue, 3)) + "\t\t" +
                  str(round(klobucharValue, 3)))


class GraphDrawer:
    def __init__(self, forecastDelays: list, preciseDelays: list, klobucharDelays: list,
                 amountOfObservations: int) -> None:
        self.__forecastDelays: list = forecastDelays
        self.__preciseDelays: list = preciseDelays
        self.__klobucharDelays: list = klobucharDelays
        self.__amountOfObservations: int = amountOfObservations

    def showDelays(self) -> None:
        forecastValues: list = []
        preciseValues: list = []
        klobucharValues: list = []
        observations = np.arange(0, self.__amountOfObservations)
        for observation in range(self.__amountOfObservations):
            forecastValue: float = self.__forecastDelays[observation].delayInMeters
            preciseValue: float = self.__preciseDelays[observation].delayInMeters
            klobucharValue: float = self.__klobucharDelays[observation].klobucharDelayInMeters
            forecastValues.append(forecastValue)
            preciseValues.append(preciseValue)
            klobucharValues.append(klobucharValue)
        plt.plot(observations * 2, forecastValues, 'o-', label="igrg")
        plt.plot(observations * 2, preciseValues, 'o-', label="igsg")
        plt.plot(observations * 2, klobucharValues, 'o-', label="Klobuchar")
        plt.locator_params(axis='x', nbins=24)
        plt.xlabel("Время, час")
        plt.ylabel("Ионосферная поправка, метр")
        plt.legend()
        plt.grid(linestyle='-', linewidth=0.5)
        plt.show()


def main():
    amountOfObservations: int = 12

    axisGeo: AxisGeo = AxisGeo(UserGeo.lonpp, IGPGeo.lon1, IGPGeo.lon2, UserGeo.latpp, IGPGeo.lat1, IGPGeo.lat2)
    weightMatrix: WeightMatrix = WeightMatrix(axisGeo)

    alphaArray: list = [0.7451E-08, -0.1490E-07, -0.5960E-07, 0.1192E-06]
    betaArray: list = [0.9216E+05, -0.1147E+06, -0.1311E+06, 0.7209E+06]
    gpsTimeArray: list = [
        80496.0, 90270.0, 93630.0, 100830.0, 108030.0, 0.0, 124230.0, 129630.0, 136800.0, 144000.0, 151200.0, 158400.0
    ]

    alpha: IonCoefficients = IonCoefficients(alphaArray)
    beta: IonCoefficients = IonCoefficients(betaArray)
    gpsTime: GpsTime = GpsTime(gpsTimeArray)

    forecastA1: list = [72, 81, 86, 73, 47, 52, 55, 57, 58, 56, 46, 38]
    forecastA2: list = [53, 84, 86, 80, 59, 54, 54, 60, 67, 57, 54, 45]
    forecastA3: list = [80, 108, 92, 98, 79, 73, 76, 76, 73, 66, 65, 55]
    forecastA4: list = [88, 108, 91, 97, 70, 72, 73, 74, 69, 63, 54, 55]

    preciseA1: list = [69, 78, 80, 64, 36, 42, 42, 52, 51, 49, 42, 37]
    preciseA2: list = [51, 80, 81, 68, 45, 47, 41, 51, 56, 51, 49, 41]
    preciseA3: list = [63, 97, 80, 87, 60, 55, 58, 59, 57, 58, 50, 39]
    preciseA4: list = [70, 98, 74, 82, 48, 51, 52, 56, 48, 49, 35, 39]

    ionosphericDelaysFactory: IonosphericDelaysFactory = IonosphericDelaysFactory(weightMatrix, amountOfObservations)
    klobucharDelaysFactory: KlobucharDelaysFactory = KlobucharDelaysFactory(gpsTime, alpha, beta, amountOfObservations)

    forecastDelays: list = ionosphericDelaysFactory.createDelays(forecastA1, forecastA2, forecastA3, forecastA4)
    preciseDelays: list = ionosphericDelaysFactory.createDelays(preciseA1, preciseA2, preciseA3, preciseA4)
    klobucharDelays: list = klobucharDelaysFactory.createKlobuchar()

    consoleOutput: ConsoleOutput = ConsoleOutput(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)
    consoleOutput.printDelays()

    graphDrawer: GraphDrawer = GraphDrawer(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)
    graphDrawer.showDelays()


if __name__ == "__main__":
    main()

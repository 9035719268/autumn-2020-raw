import matplotlib.pyplot as plt
import numpy as np


class PseudoRange:
    def __init__(self, pseudoRanges: list) -> None:
        self.__pseudoRanges: list = pseudoRanges

    def __getitem__(self, observation: int) -> float:
        return self.__pseudoRanges[observation]


class IonosphericDelay:
    def __init__(self, pseudoRange1: PseudoRange, pseudoRange2: PseudoRange) -> None:
        self.__pseudoRange1: PseudoRange = pseudoRange1
        self.__pseudoRange2: PseudoRange = pseudoRange2

    def __getitem__(self, interval: int) -> float:
        speedOfLight: float = 2.99792458 * 1E8
        p1: float = self.__pseudoRange1[interval]
        p2: float = self.__pseudoRange2[interval]
        k: float = self.__getK()
        delay: float = (p1 - p2) / (speedOfLight * (1 - k))
        delayInMeters: float = delay * speedOfLight
        return delayInMeters

    @classmethod
    def __getK(cls) -> float:
        f1: float = 1_575_420_000
        f2: float = 1_227_600_000
        k: float = pow(f1, 2) / pow(f2, 2)
        return k


class Satellite:
    def __init__(self, number: int, ionosphericDelay: IonosphericDelay) -> None:
        self.__number = number
        self.__ionosphericDelay: IonosphericDelay = ionosphericDelay

    @property
    def number(self) -> int:
        return self.__number

    @property
    def ionosphericDelay(self) -> IonosphericDelay:
        return self.__ionosphericDelay


class FileReader:
    def __init__(self, fileName: str) -> None:
        with open(fileName, "rb") as file:
            self.__allBytes: bytes = file.read()

    def getMeasurements(self, requiredSatelliteNumber: int, requiredSatelliteNumberSize: int,
                        requiredSatelliteNumber2: int = None) -> list:
        lines: list = self.__analyzeSyntaxAndReturnLinesList()
        measurements: list = []
        numbersInLine: int = 21

        for line in lines:
            try:
                satelliteNumber: int = line[0][0]
                satelliteNumberSize: int = len(line[0])
                if requiredSatelliteNumber2 is not None:
                    satelliteNumber2: int = line[0][1]
                    if (satelliteNumber is requiredSatelliteNumber and
                            satelliteNumber2 is requiredSatelliteNumber2 and
                            satelliteNumberSize is requiredSatelliteNumberSize):
                        lineOfNumbers: list = []
                        for number in range(1, numbersInLine + 1):
                            numeric: float = self.__getNumeric(line, number)
                            lineOfNumbers.append(numeric)
                        measurements.append(lineOfNumbers)
                else:
                    if (satelliteNumber is requiredSatelliteNumber and
                            satelliteNumberSize is requiredSatelliteNumberSize):
                        lineOfNumbers: list = []
                        for number in range(1, numbersInLine + 1):
                            numeric: float = self.__getNumeric(line, number)
                            lineOfNumbers.append(numeric)
                        measurements.append(lineOfNumbers)
            except Exception:
                pass
        return measurements

    @classmethod
    def __getNumeric(cls, line: list, number: int) -> float:
        numberBuilder: str = ""
        numberLength: int = len(line[number])
        for digit in range(numberLength):
            symbol: chr = chr(line[number][digit])
            numberBuilder += symbol
        numeric: float = float(numberBuilder)
        return numeric

    def __analyzeSyntaxAndReturnLinesList(self) -> list:
        lines: list = []
        words: list = []
        symbols: list = []
        isWord: bool = False

        tab: int = 9
        newLine: int = 10
        carriageReturn: int = 13
        space: int = 32

        for symbol in self.__allBytes:
            if symbol is newLine:
                words.append(symbols)
                symbols = []
                lines.append(words)
                words = []
                isWord = False
            elif isWord is True and symbol is tab:
                words.append(symbols)
                symbols = []
                isWord = False
            elif symbol is not space and symbol is not tab and symbol is not carriageReturn:
                isWord = True
                symbols.append(symbol)
        return lines


class SatelliteFactory:
    def __init__(self, fileReader: FileReader, amountOfObservations: int) -> None:
        self.__fileReader: FileReader = fileReader
        self.__amountOfObservations: int = amountOfObservations

    def createSatellite(self, satelliteNumber: str, satelliteNumberSize: int,
                        satelliteNumber2: str = None) -> Satellite:
        satelliteNumberInAscii: int = self.__toAscii(satelliteNumber)
        p1Array: list = []
        p2Array: list = []
        if satelliteNumber2 is not None:
            satelliteNumberFull: str = satelliteNumber + satelliteNumber2
            satelliteNumberNumeric = int(satelliteNumberFull)
            satelliteNumber2InAscii: int = self.__toAscii(satelliteNumber2)
            measurements: list = self.__fileReader.getMeasurements(satelliteNumberInAscii, satelliteNumberSize,
                                                                   satelliteNumber2InAscii)
            for observation in range(self.__amountOfObservations):
                p1Temp: float = measurements[observation][1]
                p1Array.append(p1Temp)
                p2Temp: float = measurements[observation][2]
                p2Array.append(p2Temp)
        else:
            satelliteNumberNumeric = int(satelliteNumber)
            measurements: list = self.__fileReader.getMeasurements(satelliteNumberInAscii, satelliteNumberSize)
            for observation in range(self.__amountOfObservations):
                p1Temp: float = measurements[observation][1]
                p1Array.append(p1Temp)
                p2Temp: float = measurements[observation][2]
                p2Array.append(p2Temp)
        p1: PseudoRange = PseudoRange(p1Array)
        p2: PseudoRange = PseudoRange(p2Array)
        delay: IonosphericDelay = IonosphericDelay(p1, p2)
        satellite: Satellite = Satellite(satelliteNumberNumeric, delay)
        return satellite

    @classmethod
    def __toAscii(cls, number: str) -> int:
        return int(number) + 48


class ConsoleOutput:
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3
        self.__amountOfObservations: int = amountOfObservations

    def printDelays(self) -> None:
        satellite1Number: int = self.__satellite1.number
        satellite2Number: int = self.__satellite2.number
        satellite3Number: int = self.__satellite3.number
        print("Ионосферная задержка\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
              "\t\tСпутник #" + str(satellite3Number))
        for observation in range(self.__amountOfObservations):
            delay1: float = self.__satellite1.ionosphericDelay[observation]
            delay2: float = self.__satellite2.ionosphericDelay[observation]
            delay3: float = self.__satellite3.ionosphericDelay[observation]
            print(str(round(delay1, 10)) + "\t" + str(round(delay2, 10)) + "\t" + str(round(delay3, 10)))


class GraphDrawer:
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3
        self.__amountOfObservations: int = amountOfObservations

    def drawDelays(self) -> None:
        satellite1Number: int = self.__satellite1.number
        satellite2Number: int = self.__satellite2.number
        satellite3Number: int = self.__satellite3.number
        satellite1Delays: list = []
        satellite2Delays: list = []
        satellite3Delays: list = []
        observations = np.arange(0, self.__amountOfObservations)
        for observation in range(self.__amountOfObservations):
            delay1: float = self.__satellite1.ionosphericDelay[observation]
            delay2: float = self.__satellite2.ionosphericDelay[observation]
            delay3: float = self.__satellite3.ionosphericDelay[observation]
            satellite1Delays.append(delay1)
            satellite2Delays.append(delay2)
            satellite3Delays.append(delay3)
        plt.plot(observations, satellite1Delays, 'o-', label="Спутник #" + str(satellite1Number))
        plt.plot(observations, satellite2Delays, 'o-', label="Спутник #" + str(satellite2Number))
        plt.plot(observations, satellite3Delays, 'o-', label="Спутник #" + str(satellite3Number))
        plt.xlabel("Время")
        plt.ylabel("Ионосферная задержка, метры")
        plt.legend()
        plt.grid(linestyle='-', linewidth=0.5)
        plt.show()


def main():
    amountOfObservations: int = 360

    fileName: str = "resources/POTS_6hours.dat"
    fileReader: FileReader = FileReader(fileName)

    satelliteFactory: SatelliteFactory = SatelliteFactory(fileReader, amountOfObservations)

    satellite1: Satellite = satelliteFactory.createSatellite("7", 1)
    satellite2: Satellite = satelliteFactory.createSatellite("2", 2, "2")
    satellite3: Satellite = satelliteFactory.createSatellite("2", 2, "3")

    consoleOutput: ConsoleOutput = ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations)
    consoleOutput.printDelays()

    graphDrawer: GraphDrawer = GraphDrawer(satellite1, satellite2, satellite3, amountOfObservations)
    graphDrawer.drawDelays()


if __name__ == '__main__':
    main()

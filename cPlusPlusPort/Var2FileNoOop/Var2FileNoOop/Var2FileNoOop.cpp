﻿#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>

auto readBytes(std::string fileName) {
	std::ifstream in(fileName);
	std::vector<char> allBytes;
	char byte;
	if (in.is_open()) {
		while (in.get(byte)) {
			allBytes.push_back(byte);
		}
	} else {
		exit(1);
	}
	in.close();
	return allBytes;
}

auto __analyzeSyntaxAndReturnLinesList(std::vector<char> allBytes) {
	auto lines = new std::vector<std::vector<std::vector<char>>>;
	auto words = new std::vector<std::vector<char>>;
	auto symbols = new std::vector<char>;
	bool isWord = false;

	char tab = 9;
	char newLine = 10;
	char carriageReturn = 13;
	char space = 32;

	for (char symbol : allBytes) {
		if (symbol == newLine) {
			words->push_back(*symbols);
			delete symbols;
			symbols = new std::vector<char>;
			lines->push_back(*words);
			delete words;
			words = new std::vector<std::vector<char>>;
			isWord = false;
		} else if ((isWord) && (symbol == tab)) {
			words->push_back(*symbols);
			delete symbols;
			symbols = new std::vector<char>;
			isWord = false;
		} else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
			isWord = true;
			symbols->push_back(symbol);
		}
	}
	delete symbols;
	delete words;
	return lines;
}

double __getNumeric(std::vector<std::vector<char>>& line, int number) {
	std::string numberBuilder = "";
	int numberLength = line.at(number).size();

	for (int digit = 0; digit < numberLength; digit++) {
		char symbol = (char)line.at(number).at(digit);
		numberBuilder += symbol;
	}

	double numeric = std::stod(numberBuilder);
	return numeric;
}

auto __getMeasurements(std::vector<char> allBytes, char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
	auto lines = __analyzeSyntaxAndReturnLinesList(allBytes);
	std::vector<std::vector<double>> measurements;
	int numbersInLine = 21;

	for (auto line : *lines) {
		try {
			char satelliteNumber1 = line.at(0).at(0);
			char satelliteNumber2 = line.at(0).at(1);
			int satelliteNumberSize = line.at(0).size();

			if ((satelliteNumber1 == requiredSatelliteNumber1) &&
				(satelliteNumber2 == requiredSatelliteNumber2) &&
				(satelliteNumberSize == requiredSatelliteNumberSize)) {
				std::vector<double> lineOfNumbers;
				for (int number = 1; number <= numbersInLine; number++) {
					double numeric = __getNumeric(line, number);
					lineOfNumbers.push_back(numeric);
				}
				measurements.push_back(lineOfNumbers);
			}
		}
		catch (std::exception ignored) {}
	}
	delete lines;
	return measurements;
}

auto __getMeasurements(std::vector<char> allBytes, char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
	auto lines = __analyzeSyntaxAndReturnLinesList(allBytes);
	std::vector<std::vector<double>> measurements;
	int numbersInLine = 21;

	for (auto line : *lines) {
		try {
			char satelliteNumber = line.at(0).at(0);
			int satelliteNumberSize = line.at(0).size();

			if ((satelliteNumber == requiredSatelliteNumber) && (satelliteNumberSize == requiredSatelliteNumberSize)) {
				std::vector<double> lineOfNumbers;
				for (int number = 1; number <= numbersInLine; number++) {
					double numeric = __getNumeric(line, number);
					lineOfNumbers.push_back(numeric);
				}
				measurements.push_back(lineOfNumbers);
			}
		}
		catch (std::exception ignored) {}
	}
	delete lines;
	return measurements;
}

auto getAngularVelocities(std::vector<char> allBytes, int amountOfObservations, char requiredSatelliteNumber1, char requiredSatelliteNumber2,
						  int requiredSatelliteNumberSize) {
	auto measurements = __getMeasurements(allBytes, requiredSatelliteNumber1, requiredSatelliteNumber2, requiredSatelliteNumberSize);
	std::vector<double> angularVelocities;
	double oneHourInSeconds = 3600;
	double previousElevationAngle = measurements.at(0).at(14);
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double currentElevationAngle = measurements.at(observation).at(14);
		double angularVelocity = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds;
		angularVelocities.push_back(angularVelocity);
		previousElevationAngle = currentElevationAngle;
	}
	return angularVelocities;
}

auto getAngularVelocities(std::vector<char> allBytes, int amountOfObservations, int requiredSatelliteNumber, int requiredSatelliteNumberSize) {
	auto measurements = __getMeasurements(allBytes, requiredSatelliteNumber, requiredSatelliteNumberSize);
	double oneHourInSeconds = 3600;
	std::vector<double> angularVelocities;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double elevationAngle = measurements.at(observation).at(14);
		double angularVelocity = elevationAngle / oneHourInSeconds;
		angularVelocities.push_back(angularVelocity);
	}
	return angularVelocities;
}

double __getVelocitySumPerHour(std::vector<double> velocities, int start, int end) {
	double velocitySum = 0;
	for (int observation = start; observation < end; observation++) {
		velocitySum += velocities.at(observation);
	}
	return velocitySum;
}

std::vector<double> __getAverageAngularVelocities(std::vector<double> angularVelocities) {
	std::vector<double> averageVelocities;
	int observationsPerHour = 120;

	double firstHourSum = __getVelocitySumPerHour(angularVelocities, 0, 120);
	double secondHourSum = __getVelocitySumPerHour(angularVelocities, 120, 240);
	double thirdHourSum = __getVelocitySumPerHour(angularVelocities, 240, 360);

	double firstHourAverage = firstHourSum / observationsPerHour;
	double secondHourAverage = secondHourSum / observationsPerHour;
	double thirdHourAverage = thirdHourSum / observationsPerHour;

	averageVelocities.push_back(firstHourAverage);
	averageVelocities.push_back(secondHourAverage);
	averageVelocities.push_back(thirdHourAverage);
	return averageVelocities;
}

auto getLinearVelocities(int amountOfObservations) {
	const double gravitational = 6.67 * pow(10, -11);
	const double earthMass = 5.972E24;
	const double earthRadius = 6371000;
	const double flightHeight = 20000;
	std::vector<double> linearVelocities;

	for (int observation = 0; observation < amountOfObservations; observation++) {
		double velocity = sqrt(gravitational * earthMass / (earthRadius + flightHeight));
		linearVelocities.push_back(velocity);
	}
	return linearVelocities;
}

std::vector<double> __getAverageLinearVelocities(int amountOfObservations) {
	std::vector<double> averageVelocities;
	std::vector<double> linearVelocities = getLinearVelocities(amountOfObservations);
	int observationsPerHour = 120;

	double firstHourSum = __getVelocitySumPerHour(linearVelocities, 0, 120);
	double secondHourSum = __getVelocitySumPerHour(linearVelocities, 120, 240);
	double thirdHourSum = __getVelocitySumPerHour(linearVelocities, 240, 360);

	double firstHourAverage = firstHourSum / observationsPerHour;
	double secondHourAverage = secondHourSum / observationsPerHour;
	double thirdHourAverage = thirdHourSum / observationsPerHour;

	averageVelocities.push_back(firstHourAverage);
	averageVelocities.push_back(secondHourAverage);
	averageVelocities.push_back(thirdHourAverage);
	return averageVelocities;
}

void __printAngularVelocities(int satellite1Number, int satellite2Number, int satellite3Number,
							  std::vector<double> satellite1Velocities, std::vector<double> satellite2Velocities,
							  std::vector<double> satellite3Velocities, int amountOfObservations) {
	std::cout << "Угловая скорость\nСпутник #" << satellite1Number << "\tСпутник #" << satellite2Number <<
				 "\tСпутник #" << satellite3Number << std::endl;
	std::cout << std::fixed << std::setprecision(10);
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double satellite1Velocity = satellite1Velocities.at(observation);
		double satellite2Velocity = satellite2Velocities.at(observation);
		double satellite3Velocity = satellite3Velocities.at(observation);
		std::cout << satellite1Velocity << "\t" << satellite2Velocity << "\t" << satellite3Velocity << std::endl;
	}
	std::cout << "***********************************************" << std::endl;
}

void __printAverageAngularVelocities(int satellite1Number, int satellite2Number, int satellite3Number,
									 std::vector<double> satellite1Velocities, std::vector<double> satellite2Velocities,
									 std::vector<double> satellite3Velocities) {
	std::cout << "Средняя угловая скорость\nСпутник #" << satellite1Number << "\t\tСпутник #" << satellite2Number <<
				 "\t\tСпутник #" << satellite3Number << std::endl;
	for (int hour = 0; hour < 3; hour++) {
		double satellite1Velocity = __getAverageAngularVelocities(satellite1Velocities).at(hour);
		double satellite2Velocity = __getAverageAngularVelocities(satellite2Velocities).at(hour);
		double satellite3Velocity = __getAverageAngularVelocities(satellite3Velocities).at(hour);
		std::cout << satellite1Velocity << "\t" << satellite2Velocity << "\t" << satellite3Velocity << std::endl;
	}
	std::cout << "***********************************************" << std::endl;
}

void __printLinearVelocities(int satellite1Number, int satellite2Number, int satellite3Number,
							 std::vector<double> satellite1Velocities, std::vector<double> satellite2Velocities,
						     std::vector<double> satellite3Velocities, int amountOfObservations) {
	std::cout << "Линейная скорость\nСпутник #" << satellite1Number << "\tСпутник #" << satellite2Number <<
				 "\tСпутник #" << satellite3Number << std::endl;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double satellite1Velocity = satellite1Velocities.at(observation);
		double satellite2Velocity = satellite2Velocities.at(observation);
		double satellite3Velocity = satellite3Velocities.at(observation);
		std::cout << satellite1Velocity << "\t" << satellite2Velocity << "\t" << satellite3Velocity << std::endl;
	}
	std::cout << "***********************************************" << std::endl;
}

void __printAverageLinearVelocities(int satellite1Number, int satellite2Number, int satellite3Number, int amountOfObservations) {
	std::cout << "Средняя линейная скорость\nСпутник #" << satellite1Number << "\t\tСпутник #" << satellite2Number <<
				 "\t\tСпутник #" << satellite3Number << std::endl;
	for (int hour = 0; hour < 3; hour++) {
		double satellite1Velocity = __getAverageLinearVelocities(amountOfObservations).at(hour);
		double satellite2Velocity = __getAverageLinearVelocities(amountOfObservations).at(hour);
		double satellite3Velocity = __getAverageLinearVelocities(amountOfObservations).at(hour);
		std::cout << satellite1Velocity << "\t" << satellite2Velocity << "\t" << satellite3Velocity << std::endl;
	}
	std::cout << "***********************************************" << std::endl;
}

void getConsoleOutput(int satellite1Number, int satellite2Number, int satellite3Number,
					  std::vector<double> satellite1AngularVelocities, std::vector<double> satellite2AngularVelocities,
					  std::vector<double> satellite3AngularVelocities, std::vector<double> satellite1LinearVelocities,
					  std::vector<double> satellite2LinearVelocities, std::vector<double> satellite3LinearVelocities,
					  int amountOfObservations) {
	__printAngularVelocities(satellite1Number, satellite2Number, satellite3Number, satellite1AngularVelocities, satellite2AngularVelocities,
							 satellite3AngularVelocities, amountOfObservations);
	__printAverageAngularVelocities(satellite1Number, satellite2Number, satellite3Number, satellite1AngularVelocities, satellite2AngularVelocities,
									satellite3AngularVelocities);
	__printLinearVelocities(satellite1Number, satellite2Number, satellite3Number, satellite1LinearVelocities, satellite2LinearVelocities,
							satellite3LinearVelocities, amountOfObservations);
	__printAverageLinearVelocities(satellite1Number, satellite2Number, satellite3Number, amountOfObservations);
}

int main() {
	setlocale(LC_ALL, "rus");
	const int amountOfObservations = 360;

	auto allBytes = readBytes("C:\\Users\\GVOZDEV\\Desktop\\cplusplusport\\cPlusPlusPort\\Var2FileNoOop\\Var2FileNoOop\\resources\\POTS_6hours.dat");

	auto satellite1AngularVelocities = getAngularVelocities(allBytes, amountOfObservations, '7', 1);
	auto satellite2AngularVelocities = getAngularVelocities(allBytes, amountOfObservations, '2', '2', 2);
	auto satellite3AngularVelocities = getAngularVelocities(allBytes, amountOfObservations, '2', '3', 2);

	auto satellite1LinearVelocities = getLinearVelocities(amountOfObservations);
	auto satellite2LinearVelocities = getLinearVelocities(amountOfObservations);
	auto satellite3LinearVelocities = getLinearVelocities(amountOfObservations);

	getConsoleOutput(7, 22, 23, satellite1AngularVelocities, satellite2AngularVelocities, satellite3AngularVelocities,
					 satellite1LinearVelocities, satellite2LinearVelocities, satellite3LinearVelocities, amountOfObservations);
}
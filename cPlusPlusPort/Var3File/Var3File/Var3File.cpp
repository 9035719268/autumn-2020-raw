﻿#define _USE_MATH_DEFINES
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>


class IonoFileReader {
private:
    std::vector<char> allBytes;

    auto analyzeSyntaxAndReturnLinesList() {
        auto lines = new std::vector<std::vector<std::vector<char>>>;
        auto words = new std::vector<std::vector<char>>;
        auto symbols = new std::vector<char>;
        bool isWord = false;

        char newLine = 10;
        char space = 32;

        for (char symbol : allBytes) {
            if (symbol == newLine) {
                words->push_back(*symbols);
                delete symbols;
                symbols = new std::vector<char>;
                lines->push_back(*words);
                delete words;
                words = new std::vector<std::vector<char>>;
                isWord = false;
            } else if ((isWord) && (symbol == space)) {
                words->push_back(*symbols);
                delete symbols;
                symbols = new std::vector<char>;
                isWord = false;
            } else if (symbol != space) {
                isWord = true;
                symbols->push_back(symbol);
            }
        }
        return lines;
    }

    int getNumeric(std::vector<std::vector<std::vector<char>>>* lines, int line, int lineWithTec, int number) {
        std::string numberBuilder = "";
        int numberLength = lines->at(line + lineWithTec).at(number).size();

        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char)lines->at(line + lineWithTec).at(number).at(digit);
            numberBuilder += symbol;
        }
        int numeric = std::stoi(numberBuilder);
        return numeric;
    }

    auto getNumberLine(std::vector<std::vector<std::vector<char>>>* lines, int line, int lineWithTec) {
        std::vector<int> numbersLine;
        int numbersInRow = lines->at(line + lineWithTec).size();

        for (int number = 0; number < numbersInRow; number++) {
            int numeric = getNumeric(lines, line, lineWithTec, number);
            numbersLine.push_back(numeric);
        }
        return numbersLine;
    }

public:
    IonoFileReader(std::string fileName) {
        std::ifstream in(fileName);
        char byte;
        if (in.is_open()) {
            while (in.get(byte)) {
                allBytes.push_back(byte);
            }
        } else {
            exit(1);
        }
        in.close();
    }

    auto getTecArray(char requiredFirstLatDigit, char requiredSecondLatDigit, char requiredThirdLatDigit, int firstLine) {
        auto lines = analyzeSyntaxAndReturnLinesList();
        std::vector<std::vector<std::vector<int>>> tecArray;
        std::vector<std::vector<int>> tecPerLat;
        std::vector<int> numbersLine;
        for (unsigned int line = firstLine; line < lines->size(); line++) {
            try {
                char firstDigitOfLat = lines->at(line).at(0).at(0);
                char secondDigitOfLat = lines->at(line).at(0).at(1);
                char thirdDigitOfLat = lines->at(line).at(0).at(2);
                int linesWithTecPerLat = 5;

                if ((firstDigitOfLat == requiredFirstLatDigit) &&
                    (secondDigitOfLat == requiredSecondLatDigit) &&
                    (thirdDigitOfLat == requiredThirdLatDigit)) {
                    std::vector<std::vector<int>> tecPerLat;
                    for (int lineWithTec = 1; lineWithTec <= linesWithTecPerLat; lineWithTec++) {
                        numbersLine = getNumberLine(lines, line, lineWithTec);
                        tecPerLat.push_back(numbersLine);
                    }
                    tecArray.push_back(tecPerLat);
                }
            }
            catch (std::exception ignored) {}
        }
        delete lines;
        return tecArray;
    }
};


class EphemerisFileReader {
private:
    std::vector<char> allBytes;
    int amountOfObservations;

    auto analyzeSyntaxAndReturnLinesList() {
        auto lines = new std::vector<std::vector<std::vector<char>>>;
        auto words = new std::vector<std::vector<char>>;
        auto symbols = new std::vector<char>;
        bool isWord = false;

        char newLine = 10;
        char space = 32;

        for (char symbol : allBytes) {
            if (symbol == newLine) {
                words->push_back(*symbols);
                delete symbols;
                symbols = new std::vector<char>;
                lines->push_back(*words);
                delete words;
                words = new std::vector<std::vector<char>>;
                isWord = false;
            } else if ((isWord) && (symbol == space)) {
                words->push_back(*symbols);
                delete symbols;
                symbols = new std::vector<char>;
                isWord = false;
            } else if (symbol != space) {
                isWord = true;
                symbols->push_back(symbol);
            }
        }
        return lines;
    }

    double getGpsTimeNumeric(std::vector<std::vector<std::vector<char>>>* lines, int observation) {
        std::string numberBuilder = "";
        int observationOffset = 7;
        int digits = lines->at(observation + observationOffset).at(0).size();

        for (int digit = 0; digit < digits; digit++) {
            char symbol = (char)lines->at(observation + observationOffset).at(0).at(digit);

            if (symbol == 'D')
                numberBuilder += 'E';
            else
                numberBuilder += symbol;
        }
        double numeric = std::stod(numberBuilder);
        return numeric;
    }

    double getCoeffsNumeric(std::vector<std::vector<char>>* line, int number) {
        std::string numberBuilder = "";
        int digits = line->at(number).size();

        for (int digit = 0; digit < digits; digit++) {
            char symbol = (char)line->at(number).at(digit);

            if (symbol == 'D')
                numberBuilder += 'E';
            else
                numberBuilder += symbol;
        }
        double numeric = std::stod(numberBuilder);
        return numeric;
    }

    auto extractIonCoeffs(int lineNumber) {
        auto lines = analyzeSyntaxAndReturnLinesList();
        auto line = &lines->at(lineNumber);
        std::vector<double> coeffs;
        int amountOfCoeffs = 4;

        for (int coeff = 0; coeff < amountOfCoeffs; coeff++) {
            double numeric = getCoeffsNumeric(line, coeff);
            coeffs.push_back(numeric);
        }
        delete lines;
        return coeffs;
    }

public:
    EphemerisFileReader(std::string fileName, int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
        std::ifstream in(fileName);
        char byte;
        if (in.is_open()) {
            while (in.get(byte)) {
                allBytes.push_back(byte);
            }
        } else {
            exit(1);
        }
        in.close();
    }

    auto getGpsTimeArray(char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
        auto lines = analyzeSyntaxAndReturnLinesList();
        std::vector<double> gpsTimeArray(amountOfObservations, 0);
        int startOfObservations = 8;
        int linesPerObservation = 8;
        int asciiStarts = 48;

        for (unsigned int observation = startOfObservations; observation < lines->size(); observation += linesPerObservation) {
            try {
                char satelliteNumber1 = lines->at(observation).at(0).at(0);
                char satelliteNumber2 = lines->at(observation).at(0).at(1);
                int satelliteNumberSize = lines->at(observation).at(0).size();
                int hourFirstNumber = (int)lines->at(observation).at(4).at(0) - asciiStarts;

                if ((lines->at(observation).at(4).size() == 1) &&
                    (hourFirstNumber % 2 == 0) &&
                    (satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    double numeric = getGpsTimeNumeric(lines, observation);
                    gpsTimeArray.at(hourFirstNumber / 2) = numeric;
                } else {
                    int hourSecondNumber = (int)lines->at(observation).at(4).at(1) - asciiStarts;
                    std::string result = std::to_string(hourFirstNumber) + std::to_string(hourSecondNumber);
                    int value = std::stoi(result);

                    if ((value % 2 == 0) &&
                        (satelliteNumber1 == requiredSatelliteNumber1) &&
                        (satelliteNumber2 == requiredSatelliteNumber2) &&
                        (satelliteNumberSize == requiredSatelliteNumberSize)) {
                        double numeric = getGpsTimeNumeric(lines, observation);
                        gpsTimeArray.at(value / 2) = numeric;
                    }
                }
            }
            catch (std::exception ignored) {}
        }
        delete lines;
        return gpsTimeArray;
    }

    std::vector<double> getGpsTimeArray(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        auto lines = analyzeSyntaxAndReturnLinesList();
        std::vector<double> gpsTimeArray(amountOfObservations, 0);
        int startOfObservations = 8;
        int linesPerObservation = 8;
        int asciiStarts = 48;

        for (unsigned int observation = startOfObservations; observation < lines->size(); observation += linesPerObservation) {
            try {
                char satelliteNumber = lines->at(observation).at(0).at(0);
                int satelliteNumberSize = lines->at(observation).at(0).size();
                int hourFirstNumber = (int)lines->at(observation).at(4).at(0) - asciiStarts;

                if ((lines->at(observation).at(4).size() == 1) &&
                    (hourFirstNumber % 2 == 0) &&
                    (satelliteNumber == requiredSatelliteNumber) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    double numeric = getGpsTimeNumeric(lines, observation);
                    gpsTimeArray.at(hourFirstNumber / 2) = numeric;
                } else {
                    int hourSecondNumber = (int)lines->at(observation).at(4).at(1) - asciiStarts;
                    std::string result = std::to_string(hourFirstNumber) + std::to_string(hourSecondNumber);
                    int value = std::stoi(result);

                    if ((value % 2 == 0) &&
                        (satelliteNumber == requiredSatelliteNumber) &&
                        (satelliteNumberSize == requiredSatelliteNumberSize)) {
                        double numeric = getGpsTimeNumeric(lines, observation);
                        gpsTimeArray.at(value / 2) = numeric;
                    }
                }
            }
            catch (std::exception ignored) {}
        }
        delete lines;
        return gpsTimeArray;
    }

    std::vector<double> getAlpha() {
        std::vector<double> alpha = extractIonCoeffs(3);
        return alpha;
    }

    std::vector<double> getBeta() {
        std::vector<double> beta = extractIonCoeffs(4);
        return beta;
    }
};



class UserGeo {
public:
    UserGeo() {
        double halfCircle = 180;
        latpp = 56.010569 / halfCircle;
        lonpp = 92.852545 / halfCircle;
    }

    double getLatpp() {
        return latpp;
    }

    double getLonpp() {
        return lonpp;
    }

private:
    double latpp;
    double lonpp;
};



class IgpGeo {
public:
    IgpGeo() {
        double halfCircle = 180;
        lon1 = 90 / halfCircle;
        lon2 = 95 / halfCircle;
        lat1 = 55 / halfCircle;
        lat2 = 57.5 / halfCircle;
    }

    double getLon1() {
        return lon1;
    }

    double getLon2() {
        return lon2;
    }

    double getLat1() {
        return lat1;
    }

    double getLat2() {
        return lat2;
    }

private:
    double lon1;
    double lon2;
    double lat1;
    double lat2;
};


class AxisGeo {
public:
    AxisGeo(double lonpp, double lon1, double lon2, double latpp, double lat1, double lat2) {
        xpp = (lonpp - lon1) / (lon2 - lon1);
        ypp = (latpp - lat1) / (lat2 - lat1);
    }

    double getXpp() {
        return xpp;
    }

    double getYpp() {
        return ypp;
    }

private:
    double xpp;
    double ypp;
};


class WeightMatrix {
public:
    WeightMatrix(AxisGeo& axisGeo) {
        this->axisGeo = &axisGeo;
        xpp = axisGeo.getXpp();
        ypp = axisGeo.getYpp();
        weights.at(0) = xpp * ypp;
        weights.at(1) = (1 - xpp) * ypp;
        weights.at(2) = (1 - xpp) * (1 - ypp);
        weights.at(3) = xpp * (1 - ypp);
    }

    double getWeightAt(int pos) {
        return weights.at(pos);
    }

private:
    AxisGeo* axisGeo;
    std::vector<double> weights;
    double xpp;
    double ypp;
};



class IonCoefficients {
public:
    IonCoefficients(std::vector<double> coefficients) {
        this->coefficients = coefficients;
    }

    double getCoefficientAt(int pos) {
        return coefficients.at(pos);
    }

private:
    std::vector<double> coefficients;
};


class GpsTime {
public:
    GpsTime(std::vector<double> gpsTime) {
        this->gpsTime = gpsTime;
    }

    double getGpsTimeAt(int pos) {
        return gpsTime.at(pos);
    }

private:
    std::vector<double> gpsTime;
};


class IonosphericDelay {
public:
    IonosphericDelay(WeightMatrix& weightMatrix, std::vector<int> tecOnAngles) {
        this->weightMatrix = &weightMatrix;
        this->tecOnAngles = tecOnAngles;
    }

    double getDelayInMeters() {
        double tecuToMetersCoefficient = getTecuToMetersCoefficient();
        double delayInTecu = getDelayInTecu();
        double delayInMeters = delayInTecu * tecuToMetersCoefficient;
        return delayInMeters;
    }

private:
    WeightMatrix* weightMatrix;
    std::vector<int> tecOnAngles;
    double getTecuToMetersCoefficient() {
        double l1 = 1575420000;
        double oneTecUnit = 1E16;
        double coefficient = 40.3 / pow(l1, 2) * oneTecUnit;
        return coefficient;
    }

    double getDelayInTecu() {
        double delay = 0;
        for (int interval = 0; interval < 4; interval++) {
            double weight = weightMatrix->getWeightAt(interval);
            double rawTec = tecOnAngles.at(interval);
            double tecInOneTecUnit = rawTec * 0.1;
            delay += (weight * tecInOneTecUnit);
        }
        return delay;
    }
};


class KlobucharModel {
public:
    KlobucharModel(double gpsTime, IonCoefficients& alphaN, IonCoefficients& betaN, UserGeo& userGeo) {
        double halfCircle = 180;
        this->gpsTime = gpsTime;
        this->elevationAngle = 90 / halfCircle;
        this->azimuth = 0;
        this->alphaN = &alphaN;
        this->betaN = &betaN;
        this->userGeo = &userGeo;
    }

    double getKlobucharDelayInMeters() {
        double delayInSeconds = getKlobucharDelayInSeconds();
        double speedOfLight = 2.99792458 * 1E8;
        double delayInMeters = delayInSeconds * speedOfLight;
        return delayInMeters;
    }

private:
    IonCoefficients* alphaN;
    IonCoefficients* betaN;
    UserGeo* userGeo;
    double elevationAngle, azimuth, gpsTime;

    double getEarthCenteredAngle() {
        double earthCenteredAngle = 0.0137 / (elevationAngle + 0.11) - 0.022;
        return earthCenteredAngle;
    }

    double getIppLatitude() {
        double latpp = userGeo->getLatpp();
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = latpp + earthCenteredAngle * cos(azimuth);
        if (ippLatitude > 0.416)
            ippLatitude = 0.416;
        else if (ippLatitude < -0.416)
            ippLatitude = -0.416;
        return ippLatitude;
    }

    double getIppLongtitude() {
        double lonpp = userGeo->getLonpp();
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = getIppLatitude();
        double ippLongtitude = lonpp + (earthCenteredAngle * sin(azimuth) / (cos(ippLatitude)));
        return ippLongtitude;
    }

    double getIppGeomagneticLatitude() {
        double ippLatitude = getIppLatitude();
        double ippLongtitude = getIppLongtitude();
        double ippGeomagneticLatitude = ippLatitude + 0.064 * cos(ippLongtitude - 1.617);
        return ippGeomagneticLatitude;
    }

    double getIppLocalTime() {
        double secondsInOneDay = 86400;
        double secondsInTwelveHours = 43200;
        double ippLongtitude = getIppLongtitude();
        double ippLocalTime = secondsInTwelveHours * ippLongtitude + gpsTime;
        while (ippLocalTime > secondsInOneDay)
            ippLocalTime -= secondsInOneDay;
        while (ippLocalTime < 0)
            ippLocalTime += secondsInOneDay;
        return ippLocalTime;
    }

    double getIonosphericDelayAmplitude() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        double amplitude = 0;
        for (int i = 0; i < 4; i++) {
            amplitude += (alphaN->getCoefficientAt(i) * pow(ippGeomagneticLatitude, i));
        }
        if (amplitude < 0)
            amplitude = 0;
        return amplitude;
    }

    double getIonosphericDelayPeriod() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        double period = 0;
        for (int i = 0; i < 4; i++) {
            period += (betaN->getCoefficientAt(i) * pow(ippGeomagneticLatitude, i));
        }
        if (period < 72000)
            period = 72000;
        return period;
    }

    double getIonosphericDelayPhase() {
        double ippLocalTime = getIppLocalTime();
        double ionosphericDelayPeriod = getIonosphericDelayPeriod();
        double ionosphericDelayPhase = 2 * M_PI * (ippLocalTime - 50400) / ionosphericDelayPeriod;
        return ionosphericDelayPhase;
    }

    double getSlantFactor() {
        double slantFactor = 1 + 16 * pow((0.53 - elevationAngle), 3);
        return slantFactor;
    }

    double getKlobucharDelayInSeconds() {
        double ionosphericDelayPhase = getIonosphericDelayPhase();
        double ionosphericDelayAmplitude = getIonosphericDelayAmplitude();
        double slantFactor = getSlantFactor();
        double ionosphericTimeDelay = 0;
        if (abs(ionosphericDelayPhase) > 1.57)
            ionosphericTimeDelay = 5E-9 * slantFactor;
        else
            ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - pow(ionosphericDelayPhase, 2) / 2 + pow(ionosphericDelayPhase, 4) / 24)) * slantFactor;
        return ionosphericTimeDelay;
    }
};



class IonosphericDelaysFactory {
public:
    IonosphericDelaysFactory(WeightMatrix& weightMatrix, int lon1, int lon2, int lon3, int lon4, int amountOfObservations) {
        this->weightMatrix = &weightMatrix;
        this->lon1 = lon1;
        this->lon2 = lon2;
        this->lon3 = lon3;
        this->lon4 = lon4;
        this->amountOfObservations = amountOfObservations;
        rowA1 = rowA2 = rowA3 = rowA4 = posA1 = posA2 = posA3 = posA4 = 0;
    }

    auto createDelays(std::vector<std::vector<std::vector<int>>> tecA1,
        std::vector<std::vector<std::vector<int>>> tecA2,
        std::vector<std::vector<std::vector<int>>> tecA3,
        std::vector<std::vector<std::vector<int>>> tecA4) {
        setRows();
        setPos();
        std::vector<IonosphericDelay> delays;

        for (int observation = 0; observation < amountOfObservations; observation++) {
            int a1 = tecA1.at(observation).at(rowA1).at(posA1);
            int a2 = tecA2.at(observation).at(rowA2).at(posA2);
            int a3 = tecA3.at(observation).at(rowA3).at(posA3);
            int a4 = tecA4.at(observation).at(rowA4).at(posA4);

            std::vector<int> tecOnAngles;
            tecOnAngles.push_back(a1);
            tecOnAngles.push_back(a2);
            tecOnAngles.push_back(a3);
            tecOnAngles.push_back(a4);

            IonosphericDelay tempDelay(*weightMatrix, tecOnAngles);
            delays.push_back(tempDelay);
        }
        return delays;
    }

private:
    const int lonFirst = -180;
    const int dlon = 5;
    const int tecValuesPerLine = 16;
    int lon1, lon2, lon3, lon4, rowA1, rowA2, rowA3, rowA4, posA1, posA2, posA3, posA4, amountOfObservations;
    WeightMatrix* weightMatrix;

    int getRow(int lon) {
        int row = abs(lonFirst - lon) / (tecValuesPerLine * dlon);
        return row;
    }

    void setRows() {
        rowA1 = getRow(lon1);
        rowA2 = getRow(lon2);
        rowA3 = getRow(lon3);
        rowA4 = getRow(lon4);
    }

    int getPos(int row, int lon) {
        int number = abs(((lonFirst - lon) / dlon)) - (row * tecValuesPerLine);
        return number;
    }

    void setPos() {
        posA1 = getPos(rowA1, lon1);
        posA2 = getPos(rowA2, lon2);
        posA3 = getPos(rowA3, lon3);
        posA4 = getPos(rowA4, lon4);
    }
};


class KlobucharDelaysFactory {
public:
    KlobucharDelaysFactory(GpsTime& gpsTime, IonCoefficients& alpha, IonCoefficients& beta, UserGeo& userGeo, int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
        this->gpsTime = &gpsTime;
        this->alpha = &alpha;
        this->beta = &beta;
        this->userGeo = &userGeo;
    }

    std::vector<KlobucharModel> createKlobuchar() {
        std::vector<KlobucharModel> models;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double time = gpsTime->getGpsTimeAt(observation);
            KlobucharModel klobucharTemp(time, *alpha, *beta, *userGeo);
            models.push_back(klobucharTemp);
        }
        return models;
    }

private:
    GpsTime* gpsTime;
    IonCoefficients* alpha;
    IonCoefficients* beta;
    UserGeo* userGeo;
    int amountOfObservations;
};


class ConsoleOutput {
public:
    ConsoleOutput(std::vector<IonosphericDelay> forecastValues, std::vector<IonosphericDelay> preciseValues,
                  std::vector<KlobucharModel> klobucharValues, int amountOfObservations) {
        this->forecastValues = forecastValues;
        this->preciseValues = preciseValues;
        this->klobucharValues = klobucharValues;
        this->amountOfObservations = amountOfObservations;
    }

    void printDelays() {
        std::cout << "igrg\t\tigsg\t\tklobuchar" << std::endl;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double forecastValue = forecastValues.at(observation).getDelayInMeters();
            double preciseValue = preciseValues.at(observation).getDelayInMeters();
            double klobucharValue = klobucharValues.at(observation).getKlobucharDelayInMeters();

            std::cout << std::fixed << std::setprecision(3) <<
                forecastValue << "\t\t" << preciseValue << "\t\t" << klobucharValue << std::endl;
        }
    }

private:
    std::vector<IonosphericDelay> forecastValues;
    std::vector<IonosphericDelay> preciseValues;
    std::vector<KlobucharModel> klobucharValues;
    int amountOfObservations;
};


int main() {
    setlocale(LC_ALL, "rus");
    int amountOfObservations = 12;

    UserGeo userGeo;
    IgpGeo igpGeo;

    double lonpp = userGeo.getLonpp();
    double lon1 = igpGeo.getLon1();
    double lon2 = igpGeo.getLon2();
    double latpp = userGeo.getLatpp();
    double lat1 = igpGeo.getLat1();
    double lat2 = igpGeo.getLat2();

    AxisGeo axisGeo(lonpp, lon1, lon2, latpp, lat1, lat2);
    WeightMatrix weightMatrix(axisGeo);

    EphemerisFileReader ephemerisFileReader("C:\\Users\\GVOZDEV\\Desktop\\cplusplusport\\cPlusPlusPort\\Var3File\\Var3File\\resources\\brdc0010.18n", amountOfObservations);
    auto gpsTimeArray = ephemerisFileReader.getGpsTimeArray('7', 1);
    IonCoefficients alpha(ephemerisFileReader.getAlpha());
    IonCoefficients beta(ephemerisFileReader.getBeta());
    GpsTime gpsTime(gpsTimeArray);

    IonoFileReader ionoFileReaderForecast("C:\\Users\\GVOZDEV\\Desktop\\cplusplusport\\cPlusPlusPort\\Var3File\\Var3File\\resources\\igrg0010.18i");
    auto forecastA1 = ionoFileReaderForecast.getTecArray('5', '7', '.', 304);
    auto forecastA2 = ionoFileReaderForecast.getTecArray('5', '7', '.', 304);
    auto forecastA3 = ionoFileReaderForecast.getTecArray('5', '5', '.', 304);
    auto forecastA4 = ionoFileReaderForecast.getTecArray('5', '5', '.', 304);

    IonoFileReader ionoFileReaderPrecise("C:\\Users\\GVOZDEV\\Desktop\\cplusplusport\\cPlusPlusPort\\Var3File\\Var3File\\resources\\igsg0010.18i");
    auto preciseA1 = ionoFileReaderPrecise.getTecArray('5', '7', '.', 394);
    auto preciseA2 = ionoFileReaderPrecise.getTecArray('5', '7', '.', 394);
    auto preciseA3 = ionoFileReaderPrecise.getTecArray('5', '5', '.', 394);
    auto preciseA4 = ionoFileReaderPrecise.getTecArray('5', '5', '.', 394);

    IonosphericDelaysFactory ionosphericDelaysFactory(weightMatrix, 95, 90, 90, 95, amountOfObservations);
    KlobucharDelaysFactory klobucharDelaysFactory(gpsTime, alpha, beta, userGeo, amountOfObservations);

    auto forecastValues = ionosphericDelaysFactory.createDelays(forecastA1, forecastA2, forecastA3, forecastA4);
    auto preciseValues = ionosphericDelaysFactory.createDelays(preciseA1, preciseA2, preciseA3, preciseA4);
    auto klobucharValues = klobucharDelaysFactory.createKlobuchar();

    ConsoleOutput consoleOutput(forecastValues, preciseValues, klobucharValues, amountOfObservations);
    consoleOutput.printDelays();
}
﻿#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <iomanip>

class FileReader {
private:
    std::vector<char> allBytes;

    /*
     * Собирает целое число из байтов.
     *
     * @param line:		строка файла
     * @param number:	порядковый номер передаваемого измерения
     * @return			целое число
     */
    double getNumeric(std::vector<std::vector<char>>& line, int number) {
        std::string numberBuilder = "";
        int numberLength = line.at(number).size();

        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char)line.at(number).at(digit);
            numberBuilder += symbol;
        }

        double numeric = std::stod(numberBuilder);
        return numeric;
    }

    /*
     * Анализирует синтаксис входного файла и возвращает список его строк.
     * Сам список строк в себе содержит списки слов, которые, в свою очередь, содержат списки символов.
     * Анализатор делит текст на слова на основании ASCII-кодов символов:
     * - табуляция (ASCII-код: 9);
     * - новая строка (ASCII-код: 10);
     * - возврат курсора (ASCII-код: 13);
     * - пробел (ASCII-код: 32)
     *
     * @return список строк входного файла
     */
    auto analyzeSyntaxAndReturnLinesList() {
        auto lines = new std::vector<std::vector<std::vector<char>>>;
        auto words = new std::vector<std::vector<char>>;
        auto symbols = new std::vector<char>;
        bool isWord = false;

        char tab = 9;
        char newLine = 10;
        char carriageReturn = 13;
        char space = 32;

        for (char symbol : allBytes) {
            if (symbol == newLine) {
                words->push_back(*symbols);
                delete symbols;
                symbols = new std::vector<char>;
                lines->push_back(*words);
                delete words;
                words = new std::vector<std::vector<char>>;
                isWord = false;
            } else if ((isWord) && (symbol == tab)) {
                words->push_back(*symbols);
                delete symbols;
                symbols = new std::vector<char>;
                isWord = false;
            } else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
                isWord = true;
                symbols->push_back(symbol);
            }
        }
        delete symbols;
        delete words;
        return lines;
    }

public:
    FileReader(std::string fileName) {
        std::ifstream in(fileName);
        char byte;
        if (in.is_open()) {
            while (in.get(byte)) {
                allBytes.push_back(byte);
            }
        } else {
            exit(1);
        }
        in.close();
    }

    /*
     * Читает измерения, полученные спутником.
     * При попытке обращения ко второму числу однозначного порядкового номера спутника выбрасывается исключение, которое игнорируется.
     *
     * @param requiredSatelliteNumber1:		первая цифра номера спутника
     * @param requiredSatelliteNumber2:		вторая цифра номера спутника (если есть)
     * @param requiredSatelliteNumberSize:	размер цифры номера спутника (требуется для корректного поиска по номеру спутника)
     */
    auto getMeasurements(char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
        auto lines = analyzeSyntaxAndReturnLinesList();
        std::vector<std::vector<double>> measurements;
        int numbersInLine = 21;

        for (auto line : *lines) {
            try {
                char satelliteNumber1 = line.at(0).at(0);
                char satelliteNumber2 = line.at(0).at(1);
                int satelliteNumberSize = line.at(0).size();

                if ((satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    std::vector<double> lineOfNumbers;
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.push_back(numeric);
                    }
                    measurements.push_back(lineOfNumbers);
                }
            }
            catch (std::exception ignored) {}
        }
        delete lines;
        return measurements;
    }

    auto getMeasurements(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        auto lines = analyzeSyntaxAndReturnLinesList();
        std::vector<std::vector<double>> measurements;
        int numbersInLine = 21;

        for (auto line : *lines) {
            try {
                char satelliteNumber = line.at(0).at(0);
                int satelliteNumberSize = line.at(0).size();

                if ((satelliteNumber == requiredSatelliteNumber) && (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    std::vector<double> lineOfNumbers;
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.push_back(numeric);
                    }
                    measurements.push_back(lineOfNumbers);
                }
            }
            catch (std::exception ignored) {}
        }
        delete lines;
        return measurements;
    }
};


class PseudoRange {
public:
    PseudoRange(std::vector<double> pseudoRanges) {
        this->pseudoRanges = pseudoRanges;
    }

    double getPseudoRangeAt(int observation) {
        return pseudoRanges.at(observation);
    }

private:
    std::vector<double> pseudoRanges;
};


class IonosphericDelay {
public:
    IonosphericDelay(PseudoRange& pseudoRange1, PseudoRange& pseudoRange2) {
        this->pseudoRange1 = &pseudoRange1;
        this->pseudoRange2 = &pseudoRange2;
    }

    double getIonosphericDelayAt(int observation) {
        double speedOfLight = 2.99792458 * 1E8;
        double p1 = pseudoRange1->getPseudoRangeAt(observation);
        double p2 = pseudoRange2->getPseudoRangeAt(observation);
        double k = getK();
        double delay = (p1 - p2) / (speedOfLight * (1 - k));
        double delayInMeters = delay * speedOfLight;
        return delayInMeters;
    }

private:
    PseudoRange* pseudoRange1;
    PseudoRange* pseudoRange2;
    std::vector<double> ionosphericDelays;

    double getK() {
        double f1 = 1575420000;
        double f2 = 1227600000;
        double k = pow(f1, 2) / pow(f2, 2);
        return k;
    }
};


class Satellite {
public:
    Satellite(int number, IonosphericDelay& ionosphericDelay) {
        this->number = number;
        this->ionosphericDelay = &ionosphericDelay;
    }

    int getNumber() {
        return number;
    }

    double getIonosphericDelayAt(int observation) {
        return ionosphericDelay->getIonosphericDelayAt(observation);
    }

private:
    int number;
    IonosphericDelay* ionosphericDelay;
};



class SatelliteFactory {
public:
    SatelliteFactory(FileReader& fileReader, int amountOfObservations) {
        this->fileReader = &fileReader;
        this->amountOfObservations = amountOfObservations;
    }

    Satellite* createSatellite(char satelliteNumber1, char satelliteNumber2, int satelliteNumberSize) {
        std::string satelliteNumber = std::string(1, satelliteNumber1) + satelliteNumber2;
        int satelliteNumeric = std::stoi(satelliteNumber);
        auto measurements = fileReader->getMeasurements(satelliteNumber1, satelliteNumber2, satelliteNumberSize);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double p1Temp = measurements.at(observation).at(1);
            double p2Temp = measurements.at(observation).at(2);
            p1Arr.push_back(p1Temp);
            p2Arr.push_back(p2Temp);
        }
        PseudoRange* p1 = new PseudoRange(p1Arr);
        PseudoRange* p2 = new PseudoRange(p2Arr);
        IonosphericDelay* delay = new IonosphericDelay(*p1, *p2);
        Satellite* satellite = new Satellite(satelliteNumeric, *delay);
        return satellite;
    }
	
	Satellite* createSatellite(char satelliteNumber, int satelliteNumberSize) {
        int satelliteNumeric = satelliteNumber - 48;
        auto measurements = fileReader->getMeasurements(satelliteNumber, satelliteNumberSize);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double p1Temp = measurements.at(observation).at(1);
            p1Arr.push_back(p1Temp);
            double p2Temp = measurements.at(observation).at(2);
            p2Arr.push_back(p2Temp);
        }
        PseudoRange* p1 = new PseudoRange(p1Arr);
        PseudoRange* p2 = new PseudoRange(p2Arr);
        IonosphericDelay* delay = new IonosphericDelay(*p1, *p2);
        Satellite* satellite = new Satellite(satelliteNumeric, *delay);
        return satellite;
    }

private:
    int amountOfObservations;
    FileReader* fileReader;
    std::vector<double> p1Arr;
    std::vector<double> p2Arr;
    std::vector<double> p3Arr;
};


class ConsoleOutput {
public:
    ConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
        this->amountOfObservations = amountOfObservations;
    }

    void printDelays() {
        int satellite1Number = satellite1->getNumber();
        int satellite2Number = satellite2->getNumber();
        int satellite3Number = satellite3->getNumber();
        std::cout << "Ионосферная задержка\nСпутник #" << satellite1Number << "\t\tСпутник #" << satellite2Number <<
            "\t\tСпутник #" << satellite3Number << std::endl;
        std::cout << std::fixed << std::setprecision(10);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double delay1 = satellite1->getIonosphericDelayAt(observation);
            double delay2 = satellite2->getIonosphericDelayAt(observation);
            double delay3 = satellite3->getIonosphericDelayAt(observation);
            std::cout << delay1 << "\t" << delay2 << "\t" << delay3 << std::endl;
        }
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
    int amountOfObservations;
};


int main() {
    setlocale(LC_ALL, "rus");
    const int amountOfObservations = 360;
    FileReader fileReader("C:\\Users\\GVOZDEV\\Desktop\\cplusplusport\\cPlusPlusPort\\Var1File\\Var1File\\resources\\POTS_6hours.dat");

    SatelliteFactory satelliteFactory(fileReader, amountOfObservations);

    Satellite* satellite1 = satelliteFactory.createSatellite('7', 1);
    Satellite* satellite2 = satelliteFactory.createSatellite('2', '2', 2);
    Satellite* satellite3 = satelliteFactory.createSatellite('2', '3', 2);

    ConsoleOutput consoleOutput(*satellite1, *satellite2, *satellite3, amountOfObservations);
    consoleOutput.printDelays();
}
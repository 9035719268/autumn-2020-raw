﻿#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <iomanip>


auto readBytes(std::string fileName) {
	std::ifstream in(fileName);
	std::vector<char> allBytes;
	char byte;
	if (in.is_open()) {
		while (in.get(byte)) {
			allBytes.push_back(byte);
		}
	} else {
		exit(1);
	}
	in.close();
	return allBytes;
}

auto __analyzeSyntaxAndReturnLinesList(std::vector<char> allBytes) {
	auto lines = new std::vector<std::vector<std::vector<char>>>;
	auto words = new std::vector<std::vector<char>>;
	auto symbols = new std::vector<char>;
	bool isWord = false;

	char tab = 9;
	char newLine = 10;
	char carriageReturn = 13;
	char space = 32;

	for (char symbol : allBytes) {
		if (symbol == newLine) {
			words->push_back(*symbols);
			delete symbols;
			symbols = new std::vector<char>;
			lines->push_back(*words);
			delete words;
			words = new std::vector<std::vector<char>>;
			isWord = false;
		} else if ((isWord) && (symbol == tab)) {
			words->push_back(*symbols);
			delete symbols;
			symbols = new std::vector<char>;
			isWord = false;
		} else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
			isWord = true;
			symbols->push_back(symbol);
		}
	}
	delete symbols;
	delete words;
	return lines;
}

double __getNumeric(std::vector<std::vector<char>>& line, int number) {
	std::string numberBuilder = "";
	int numberLength = line.at(number).size();

	for (int digit = 0; digit < numberLength; digit++) {
		char symbol = (char)line.at(number).at(digit);
		numberBuilder += symbol;
	}

	double numeric = std::stod(numberBuilder);
	return numeric;
}

auto __getMeasurements(std::vector<char> allBytes, char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
	auto lines = __analyzeSyntaxAndReturnLinesList(allBytes);
	std::vector<std::vector<double>> measurements;
	int numbersInLine = 21;

	for (auto line : *lines) {
		try {
			char satelliteNumber1 = line.at(0).at(0);
			char satelliteNumber2 = line.at(0).at(1);
			int satelliteNumberSize = line.at(0).size();

			if ((satelliteNumber1 == requiredSatelliteNumber1) &&
				(satelliteNumber2 == requiredSatelliteNumber2) &&
				(satelliteNumberSize == requiredSatelliteNumberSize)) {
				std::vector<double> lineOfNumbers;
				for (int number = 1; number <= numbersInLine; number++) {
					double numeric = __getNumeric(line, number);
					lineOfNumbers.push_back(numeric);
				}
				measurements.push_back(lineOfNumbers);
			}
		}
		catch (std::exception ignored) {}
	}
	delete lines;
	return measurements;
}

auto __getMeasurements(std::vector<char> allBytes, char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
	auto lines = __analyzeSyntaxAndReturnLinesList(allBytes);
	std::vector<std::vector<double>> measurements;
	int numbersInLine = 21;

	for (auto line : *lines) {
		try {
			char satelliteNumber = line.at(0).at(0);
			int satelliteNumberSize = line.at(0).size();

			if ((satelliteNumber == requiredSatelliteNumber) && (satelliteNumberSize == requiredSatelliteNumberSize)) {
				std::vector<double> lineOfNumbers;
				for (int number = 1; number <= numbersInLine; number++) {
					double numeric = __getNumeric(line, number);
					lineOfNumbers.push_back(numeric);
				}
				measurements.push_back(lineOfNumbers);
			}
		}
		catch (std::exception ignored) {}
	}
	delete lines;
	return measurements;
}

double __getK() {
	double f1 = 1575420000;
	double f2 = 1227600000;
	double k = pow(f1, 2) / pow(f2, 2);
	return k;
}

auto getDelays(std::vector<char> allBytes, int amountOfObservations, char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
	auto measurements = __getMeasurements(allBytes, requiredSatelliteNumber1, requiredSatelliteNumber2, requiredSatelliteNumberSize);
	double speedOfLight = 2.99792458 * 10E8;
	std::vector<double> delays;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double p1 = measurements.at(observation).at(1);
		double p2 = measurements.at(observation).at(2);
		double k = __getK();
		double delay = (p1 - p2) / (speedOfLight * (1 - k));
		double delayInMeters = delay * speedOfLight;
		delays.push_back(delayInMeters);
	}
	return delays;
}

auto getDelays(std::vector<char> allBytes, int amountOfObservations, char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
	auto measurements = __getMeasurements(allBytes, requiredSatelliteNumber, requiredSatelliteNumberSize);
	double speedOfLight = 2.99792458 * 1E8;
	std::vector<double> delays;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double p1 = measurements.at(observation).at(1);
		double p2 = measurements.at(observation).at(2);
		double k = __getK();
		double delay = (p1 - p2) / (speedOfLight * (1 - k));
		delays.push_back(delay);
	}
	return delays;
}

void printDelays(int satellite1Number, int satellite2Number, int satellite3Number,
				 std::vector<double> satellite1Delays, std::vector<double> satellite2Delays, std::vector<double> satellite3Delays,
				 int amountOfObservations) {
	std::cout << "Ионосферная задержка\nСпутник #" << satellite1Number << "\t\tСпутник #" << satellite2Number <<
		"\t\tСпутник #" << satellite3Number << std::endl;
	std::cout << std::fixed << std::setprecision(10);
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double delay1 = satellite1Delays.at(observation);
		double delay2 = satellite2Delays.at(observation);
		double delay3 = satellite3Delays.at(observation);
		std::cout << delay1 << "\t" << delay2 << "\t" << delay3 << std::endl;
	}
}

int main() {
	setlocale(LC_ALL, "rus");
	const int amountOfObservations = 360;
	auto allBytes = readBytes("C:\\Users\\GVOZDEV\\Desktop\\cplusplusport\\cPlusPlusPort\\Var1FileNoOop\\Var1FileNoOop\\resources\\POTS_6hours.dat");

	auto satellite1Delays = getDelays(allBytes, amountOfObservations, '7', 1);
	auto satellite2Delays = getDelays(allBytes, amountOfObservations, '2', '2', 2);
	auto satellite3Delays = getDelays(allBytes, amountOfObservations, '2', '3', 2);

	printDelays(7, 22, 23, satellite1Delays, satellite2Delays, satellite3Delays, amountOfObservations);
}